# The name of the package:
atlas_subdir (MyJetAnalysis)

# Add the shared library:
atlas_add_library (MyJetAnalysisLib
  MyJetAnalysis/*.h Root/*.cxx
  PUBLIC_HEADERS MyJetAnalysis
  LINK_LIBRARIES PathResolver AnaAlgorithmLib EventLoop EventLoopAlgs xAODEventInfo xAODTrigger AsgAnalysisInterfaces TrigDecisionToolLib TrigConfInterfaces xAODHIEvent GoodRunsListsLib TrigConfxAODLib InDetTrackSelectionToolLib xAODJet JetCalibToolsLib JetSelectorToolsLib xAODTruth MCTruthClassifierLib xAODEgamma ElectronPhotonSelectorToolsLib ElectronPhotonFourMomentumCorrectionLib MuonSelectorToolsLib MuonMomentumCorrectionsLib)

if (XAOD_STANDALONE)
 # Add the dictionary (for AnalysisBase only):
 atlas_add_dictionary (MyJetAnalysisDict
  MyJetAnalysis/MyJetAnalysisDict.h
  MyJetAnalysis/selection.xml
  LINK_LIBRARIES MyJetAnalysisLib)
endif ()

if (NOT XAOD_STANDALONE)
  # Add a component library for AthAnalysis only:
  atlas_add_component (MyJetAnalysis
    src/components/*.cxx
    LINK_LIBRARIES MyJetAnalysisLib)
endif ()

# Install files from the package:
atlas_install_joboptions( share/*_jobOptions.py )
atlas_install_scripts( share/*_eljob.py )
atlas_install_data (share/*.*)