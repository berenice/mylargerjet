#include <AsgTools/MessageCheck.h>
#include <MyJetAnalysis/MyJetxAODAnalysis.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>

// HI event includes                                                                                             
#include "xAODHIEvent/HIEventShapeContainer.h"

//xAOD
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TStore.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODEventInfo/EventInfo.h"


//EventLoop
#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

//ROOT                                                                                                                        
#include <TFile.h>
#include <TSystem.h>
//Cross Calibration                                                                                              
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"

#include <iostream>

// E/gamma tools                                                                                                                                                                          
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
// Egamma & muons                                                                                                                                                                         
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"




MyJetxAODAnalysis :: MyJetxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
    m_trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool"),
    m_trigConfigTool("TrigConf::xAODConfigTool/xAODConfigTool")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  
  

}



StatusCode MyJetxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  //ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]                            
                                                                                                                        

  //ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));

  TFile *outputFile = wk()->getOutputFile ("ANALYSIS");
  mytree = new TTree ("tree", "tree");
  mytree->SetDirectory (outputFile);
  

  //Run & Event Number                                                                                                      
                                                                                                                             
  mytree->Branch ("RunNumber", &m_b_runNumber);
  mytree->Branch ("EventNumber", &m_b_eventNumber);



  //FCal                                                                                                                    
                                                                                                                             
  mytree->Branch ("fcalA_et", &m_b_fcalA_et, "fcalA_et/F");
  mytree->Branch ("fcalC_et", &m_b_fcalC_et, "fcalC_et/F");

  //------Triggers for Pb+Pb----------//                                                                                    
                                                                                                                             
  //HLT                                                                                                  
                                                                                                                             
  mytree->Branch("HLT_j200_a10_ion_L1J50",            &HLT_j200_a10_ion_L1J50, "HLT_j200_a10_ion_L1J50/O");
  mytree->Branch("HLT_j180_a10_ion_L1J50",            &HLT_j180_a10_ion_L1J50, "HLT_j180_a10_ion_L1J50/O");

  //------------Jets-----------                                                                                                   
  mytree->Branch ("akt10hi_jet_n",   &m_b_akt10hi_jet_n, "akt10hi_jet_n/I");
  mytree->Branch ("akt10hi_jet_pt",  &m_b_akt10hi_jet_pt);
  mytree->Branch ("akt10hi_jet_eta", &m_b_akt10hi_jet_eta);
  mytree->Branch ("akt10hi_jet_phi", &m_b_akt10hi_jet_phi);
  mytree->Branch ("akt10hi_jet_e",   &m_b_akt10hi_jet_e);



  // Initialize and configure trigger tools                                                                                    
  ANA_CHECK (m_trigConfigTool.initialize());
  ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool
                     
  ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
  ANA_CHECK (m_trigDecisionTool.initialize());






  return StatusCode::SUCCESS;
}



StatusCode MyJetxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  //HLT                                                                                                                                               
  HLT_j200_a10_ion_L1J50 = false;
  HLT_j180_a10_ion_L1J50 = false;

  //FCal                                                                                                                                              
  m_b_fcalA_et = 0;
  m_b_fcalC_et = 0;


  //Jets R = 0.2, 0.3, 0.4, 1.0                                                                                                                       
  m_b_akt10hi_jet_n = 0;
  m_b_akt10hi_jet_pt.clear ();
  m_b_akt10hi_jet_eta.clear ();
  m_b_akt10hi_jet_phi.clear ();
  m_b_akt10hi_jet_e.clear ();



  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  //Event Number & Run Number                                                                                                        
  m_b_runNumber = eventInfo->runNumber();
  m_b_eventNumber = eventInfo->eventNumber();

  




  //----------------------------------------------------------------------                                       
  // Calculated total FCal energies                                                                              
  //----------------------------------------------------------------------                                       
  
    const xAOD::HIEventShapeContainer* hiueContainer = 0;
    if (!evtStore ()->retrieve (hiueContainer, "HIEventShape").isSuccess ()) {
      Error ("GetHIEventShape ()", "Failed to retrieve HIEventShape container. Exiting." );
      return EL::StatusCode::FAILURE;
    }

    for (const auto* hiue : *hiueContainer) {
      double et = hiue->et ();
      int layer = hiue->layer ();
      double eta = hiue->etaMin ();

      if (layer == 21 || layer == 22 || layer == 23) {
        if (eta > 0)  {
          m_b_fcalA_et += et * 1e-3;
        }
        else {
          m_b_fcalC_et += et * 1e-3;
        }
      }
    }
                                                                                              


  //----------------------------------------------------------------------                                       
  // Get jets from jet container                                                                                 
  //----------------------------------------------------------------------                                       
  
  
    const xAOD::JetContainer* jet10Container = 0;                                                              
    if (!evtStore ()->retrieve (jet10Container, "AntiKt10HIJets").isSuccess ()) {                              
    Error ("GetAntiKt10HIJets ()", "Failed to retrieve AntiKt10HIJets collection. Exiting." );               
    return EL::StatusCode::FAILURE;                                                                          
    }                                                                                                          

    for (const auto* init_jet : *jet10Container) {                                                             

    const xAOD::JetFourMom_t jet_4mom_em = init_jet->jetP4 ();                                               
    const float xcalib_pt = 1e-3 * jet_4mom_em.pt ();                                                        
    const float xcalib_eta = jet_4mom_em.eta ();                                                             
    const float xcalib_phi = jet_4mom_em.phi ();                                                             
    const float xcalib_e = 1e-3 * jet_4mom_em.e ();                                                          
    //std::cout << "This the pt of your jet! " << xcalib_pt << std::endl;
    // only write jets above jets pT cut                                                                     
      if (xcalib_pt < 200) {                                                                          
        continue;                                                                                              
      }                                                                                                        

      m_b_akt10hi_jet_pt.push_back (xcalib_pt);                                                                
      m_b_akt10hi_jet_eta.push_back (xcalib_eta);                                                              
      m_b_akt10hi_jet_phi.push_back (xcalib_phi);                                                              
      m_b_akt10hi_jet_e.push_back (xcalib_e);                                                                  
      m_b_akt10hi_jet_n++;                                                                                     
    }// end jet loop                                                                                           

  
  //Triggers
  
  bool lowest_thresh_fire = false;

  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_j200_a10_ion_L1J50");
    HLT_j200_a10_ion_L1J50 = chainGroup->isPassed();
  }
  {
    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_j180_a10_ion_L1J50");
    HLT_j180_a10_ion_L1J50 = chainGroup->isPassed();
    lowest_thresh_fire = HLT_j180_a10_ion_L1J50;
  }


  //if(lowest_thresh_fire==1){tree ("analysis")->Fill ();} 



  if(lowest_thresh_fire == 1) { mytree->Fill();}

  return StatusCode::SUCCESS;
}



StatusCode MyJetxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}
