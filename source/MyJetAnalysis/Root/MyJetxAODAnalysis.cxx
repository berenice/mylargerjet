#include <AsgTools/MessageCheck.h>
#include <MyJetAnalysis/MyJetxAODAnalysis.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>

// HI event includes                                                                                             
#include "xAODHIEvent/HIEventShapeContainer.h"

//xAOD
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TStore.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODEventInfo/EventInfo.h"


//EventLoop
#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

//ROOT                                                                                                                        
#include <TFile.h>
#include <TSystem.h>
//Cross Calibration                                                                                              
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/JetCalibrationTool.h"

#include <iostream>

// E/gamma tools                                                                                                                                                                          
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
// Egamma & muons                                                                                                                                                                         
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

// Path resolver tool
#include <PathResolver/PathResolver.h>

// Tracking & vertex includes
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

// Truth includes
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"


using namespace MCTruthPartClassifier;


MyJetxAODAnalysis :: MyJetxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
    m_trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool"),
    m_trigConfigTool("TrigConf::xAODConfigTool/xAODConfigTool")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  
  m_outputName = "myOutput";  
  m_Akt10HI_Insitu_CalibTool = nullptr;
  m_Akt4HI_Insitu_CalibTool = nullptr;

  m_trackSelectionToolHITight = nullptr;
  m_trackSelectionToolHILoose = nullptr;

  m_Akt10HI_EM_EtaJES_CalibTool = nullptr;
  m_Akt4HI_EM_EtaJES_CalibTool = nullptr;
  m_trackSelectionToolTightPrimary = nullptr;
  m_truthClassifier = nullptr;

  m_jetCleaningTool = nullptr;
 
  m_jetCleaningTool_TightBad = nullptr;
 
}
/**                                                                                                                                                                                                         
 * Checks the summary value for some value.                                                                                                                                                                 
 */
uint8_t MyJetxAODAnalysis::getSum( const xAOD::TrackParticle& trk, xAOD::SummaryType sumType )
{
  //if(m_doDebug) ATH_MSG_INFO("FILE, LINE: " << __FILE__ << ", " << __LINE__);
  uint8_t sumVal=0;
  if(!trk.summaryValue(sumVal, sumType)) Error("getSum()", "Could not get summary type %i", sumType);
  return sumVal;
}


StatusCode MyJetxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  //ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]                            
                                                                                                                        

  //ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));

  sleep(2300);
 


  TFile *outputFile = wk()->getOutputFile (m_outputName);
  mytree = new TTree ("tree", "tree");
  mytree->SetDirectory (outputFile);
  

  //Run & Event Number                                                                                                      
                                                                                                                             
  mytree->Branch ("RunNumber", &m_b_runNumber);
  mytree->Branch ("EventNumber", &m_b_eventNumber);



  //FCal                                                                                                                    
                                                                                                                             
  mytree->Branch ("fcalA_et", &m_b_fcalA_et, "fcalA_et/F");
  mytree->Branch ("fcalC_et", &m_b_fcalC_et, "fcalC_et/F");

  //------Triggers-------//                                                                                    
                                                                                                                             
  //HLT            
  //Pb+Pb
  mytree->Branch("HLT_j200_a10_ion_L1J50",            &HLT_j200_a10_ion_L1J50, "HLT_j200_a10_ion_L1J50/O");
  mytree->Branch("HLT_j180_a10_ion_L1J50",            &HLT_j180_a10_ion_L1J50, "HLT_j180_a10_ion_L1J50/O");
  mytree->Branch("HLT_j150_a10_ion_L1J50",            &HLT_j150_a10_ion_L1J50, "HLT_j150_a10_ion_L1J50/O");
  //pp
  mytree->Branch("HLT_j110_a10_lcw_subjes_L1J30",     &HLT_j110_a10_lcw_subjes_L1J30, "HLT_j110_a10_lcw_subjes_L1J30/O");
  mytree->Branch("HLT_j110",                          &HLT_j110,                      "HLT_j110/O");


  
  // vertices
  mytree->Branch ("nvert",      &m_b_nvert,       "nvert/I");
  mytree->Branch ("vert_x",     &m_b_vert_x,      "vert_x[nvert]/F");
  mytree->Branch ("vert_y",     &m_b_vert_y,      "vert_y[nvert]/F");
  mytree->Branch ("vert_z",     &m_b_vert_z,      "vert_z[nvert]/F");
  mytree->Branch ("vert_ntrk",  &m_b_vert_ntrk,   "vert_ntrk[nvert]/I");
  mytree->Branch ("vert_type",  &m_b_vert_type,   "vert_type[nvert]/I");
  mytree->Branch ("vert_sumpt", &m_b_vert_sumpt,  "vert_sumpt[nvert]/F");

  //----Out-Of-Time Pile-Up 
  mytree->Branch ("isOOTPU",        &m_b_isOOTPU,       "isOOTPU/O"); 
  
  

  if(!mCollissions) {
    //------------Jets-----------                                                          
    mytree->Branch ("akt10hi_jet_n",   &m_b_akt10hi_jet_n, "akt10hi_jet_n/I");


    mytree->Branch ("LooseBad_10",            &m_b_LooseBad_10);
    mytree->Branch ("TightBad_10",            &m_b_TightBad_10);


    mytree->Branch ("akt10hi_constit_jet_pt",  &m_b_akt10hi_constit_jet_pt);  
    mytree->Branch ("akt10hi_constit_jet_eta", &m_b_akt10hi_constit_jet_eta);  
    mytree->Branch ("akt10hi_constit_jet_phi", &m_b_akt10hi_constit_jet_phi);  
    mytree->Branch ("akt10hi_constit_jet_e",   &m_b_akt10hi_constit_jet_e);   
  
    mytree->Branch ("akt10hi_em_jet_pt",  &m_b_akt10hi_em_jet_pt);  
    mytree->Branch ("akt10hi_em_jet_eta", &m_b_akt10hi_em_jet_eta);
    mytree->Branch ("akt10hi_em_jet_phi", &m_b_akt10hi_em_jet_phi); 
    mytree->Branch ("akt10hi_em_jet_e",   &m_b_akt10hi_em_jet_e);  



    //-----Calibrated Jets
    mytree->Branch ("akt10hi_jet_Insitu_calib_pt",  &m_b_akt10hi_jet_Insitu_calib_pt);  
    mytree->Branch ("akt10hi_jet_Insitu_calib_eta", &m_b_akt10hi_jet_Insitu_calib_eta);  
    mytree->Branch ("akt10hi_jet_Insitu_calib_phi", &m_b_akt10hi_jet_Insitu_calib_phi);  
    mytree->Branch ("akt10hi_jet_Insitu_calib_e",   &m_b_akt10hi_jet_Insitu_calib_e);  
    
    

    mytree->Branch ("LooseBad_4",            &m_b_LooseBad_4);
    mytree->Branch ("TightBad_4",            &m_b_TightBad_4);
 

    mytree->Branch ("akt4hi_jet_n",   &m_b_akt4hi_jet_n, "akt4hi_jet_n/I"); 

    mytree->Branch ("LooseBad_4",            &m_b_LooseBad_4);
    mytree->Branch ("TightBad_4",            &m_b_TightBad_4);
 



    mytree->Branch ("akt4hi_constit_jet_pt",  &m_b_akt4hi_constit_jet_pt);  
    mytree->Branch ("akt4hi_constit_jet_eta", &m_b_akt4hi_constit_jet_eta);  
    mytree->Branch ("akt4hi_constit_jet_phi", &m_b_akt4hi_constit_jet_phi);  
    mytree->Branch ("akt4hi_constit_jet_e",   &m_b_akt4hi_constit_jet_e);  

    mytree->Branch ("akt4hi_em_jet_pt",  &m_b_akt4hi_em_jet_pt);  
    mytree->Branch ("akt4hi_em_jet_eta", &m_b_akt4hi_em_jet_eta);  
    mytree->Branch ("akt4hi_em_jet_phi", &m_b_akt4hi_em_jet_phi);  
    mytree->Branch ("akt4hi_em_jet_e",   &m_b_akt4hi_em_jet_e); 



    //-----Calibrated Jets                                                                                                                                         
    mytree->Branch ("akt4hi_jet_Insitu_calib_pt",  &m_b_akt4hi_jet_Insitu_calib_pt);  
    mytree->Branch ("akt4hi_jet_Insitu_calib_eta", &m_b_akt4hi_jet_Insitu_calib_eta);  
    mytree->Branch ("akt4hi_jet_Insitu_calib_phi", &m_b_akt4hi_jet_Insitu_calib_phi);  
    mytree->Branch ("akt4hi_jet_Insitu_calib_e",   &m_b_akt4hi_jet_Insitu_calib_e);   







  }
  
  
  // Truth Jets                                                                                                                                                                    
  if (mCollissions) {
    //---Reco Jets
    mytree->Branch("m_b_akt10_reco_sub_et", &m_b_akt10_reco_sub_et); 
    mytree->Branch("m_b_akt10_reco_sub_e", &m_b_akt10_reco_sub_e);  
    mytree->Branch("m_b_akt10_reco_SubET", &m_b_akt10_reco_SubET);

    mytree->Branch("m_b_akt4_reco_sub_et", &m_b_akt4_reco_sub_et);   
    mytree->Branch("m_b_akt4_reco_sub_e", &m_b_akt4_reco_sub_e);  
    mytree->Branch("m_b_akt4_reco_SubET", &m_b_akt4_reco_SubET);


    mytree->Branch("m_b_akt10_reco_jet_n"          , &m_b_akt10_reco_jet_n, "m_b_akt10_reco_jet_n/I"); //------Done
    mytree->Branch("akt10_reco_jet_etaJES_calib_pt", &m_b_akt10_reco_jet_etaJES_calib_pt);  //----Done 
    mytree->Branch("akt10_reco_jet_etaJES_calib_eta", &m_b_akt10_reco_jet_etaJES_calib_eta); //------Done 
    mytree->Branch("akt10_reco_jet_etaJES_calib_phi", &m_b_akt10_reco_jet_etaJES_calib_phi); //------Done 
    mytree->Branch("akt10_reco_jet_etaJES_calib_e", &m_b_akt10_reco_jet_etaJES_calib_e); //------Done 
    
    mytree->Branch("akt10_reco_constit_jet_pt", &m_b_akt10_reco_constit_jet_pt); //------Done 
    mytree->Branch("akt10_reco_constit_jet_eta", &m_b_akt10_reco_constit_jet_eta); //------Done 
    mytree->Branch("akt10_reco_constit_jet_phi", &m_b_akt10_reco_constit_jet_phi); //------Done 
    mytree->Branch("akt10_reco_constit_jet_e", &m_b_akt10_reco_constit_jet_e); //------Done 
    
    mytree->Branch("akt10_reco_em_jet_pt", &m_b_akt10_reco_em_jet_pt); //------Done 
    mytree->Branch("akt10_reco_em_jet_eta", &m_b_akt10_reco_em_jet_eta); //------Done 
    mytree->Branch("akt10_reco_em_jet_phi", &m_b_akt10_reco_em_jet_phi); //------Done 
    mytree->Branch("akt10_reco_em_jet_e", &m_b_akt10_reco_em_jet_e); //------Done 

    mytree->Branch("akt10_reco_jet_Insitu_calib_pt", &m_b_akt10_reco_jet_Insitu_calib_pt); //------Done 
    mytree->Branch("akt10_reco_jet_Insitu_calib_eta", &m_b_akt10_reco_jet_Insitu_calib_eta); //------Done 
    mytree->Branch("akt10_reco_jet_Insitu_calib_phi", &m_b_akt10_reco_jet_Insitu_calib_phi); //------Done 
    mytree->Branch("akt10_reco_jet_Insitu_calib_e", &m_b_akt10_reco_jet_Insitu_calib_e); //------Done 



    mytree->Branch("m_b_akt4_reco_jet_n"          , &m_b_akt4_reco_jet_n, "m_b_akt4_reco_jet_n/I"); //------Done 
    mytree->Branch("akt4_reco_jet_etaJES_calib_pt", &m_b_akt4_reco_jet_etaJES_calib_pt); //------Done 
    mytree->Branch("akt4_reco_jet_etaJES_calib_eta", &m_b_akt4_reco_jet_etaJES_calib_eta); //------Done 
    mytree->Branch("akt4_reco_jet_etaJES_calib_phi", &m_b_akt4_reco_jet_etaJES_calib_phi); //------Done 
    mytree->Branch("akt4_reco_jet_etaJES_calib_e", &m_b_akt4_reco_jet_etaJES_calib_e); //------Done 

    mytree->Branch("akt4_reco_constit_jet_pt", &m_b_akt4_reco_constit_jet_pt); //------Done 
    mytree->Branch("akt4_reco_constit_jet_eta", &m_b_akt4_reco_constit_jet_eta); //------Done 
    mytree->Branch("akt4_reco_constit_jet_phi", &m_b_akt4_reco_constit_jet_phi); //------Done 
    mytree->Branch("akt4_reco_constit_jet_e", &m_b_akt4_reco_constit_jet_e); //------Done 

    mytree->Branch("akt4_reco_em_jet_pt", &m_b_akt4_reco_em_jet_pt);  //------Done 
    mytree->Branch("akt4_reco_em_jet_eta", &m_b_akt4_reco_em_jet_eta); //------Done 
    mytree->Branch("akt4_reco_em_jet_phi", &m_b_akt4_reco_em_jet_phi); //------Done 
    mytree->Branch("akt4_reco_em_jet_e", &m_b_akt4_reco_em_jet_e); //------Done 

    mytree->Branch("akt4_reco_jet_Insitu_calib_pt", &m_b_akt4_reco_jet_Insitu_calib_pt); //------Done 
    mytree->Branch("akt4_reco_jet_Insitu_calib_eta", &m_b_akt4_reco_jet_Insitu_calib_eta); //------Done 
    mytree->Branch("akt4_reco_jet_Insitu_calib_phi", &m_b_akt4_reco_jet_Insitu_calib_phi); //------Done 
    mytree->Branch("akt4_reco_jet_Insitu_calib_e", &m_b_akt4_reco_jet_Insitu_calib_e); //------Done 

    

    mytree->Branch ("akt10_truth_jet_n",   &m_b_akt10_truth_jet_n, "akt10_truth_jet_n/I");   //------Done 
    mytree->Branch ("akt10_truth_jet_pt",  &m_b_akt10_truth_jet_pt); //------Done 
    mytree->Branch ("akt10_truth_jet_eta", &m_b_akt10_truth_jet_eta); //------Done 
    mytree->Branch ("akt10_truth_jet_phi", &m_b_akt10_truth_jet_phi); //------Done 
    mytree->Branch ("akt10_truth_jet_e",   &m_b_akt10_truth_jet_e); //------Done 
    

    mytree->Branch ("akt4_truth_jet_n",   &m_b_akt4_truth_jet_n, "akt4_truth_jet_n/I");  //------Done 
    mytree->Branch ("akt4_truth_jet_pt",  &m_b_akt4_truth_jet_pt); //------Done 
    mytree->Branch ("akt4_truth_jet_eta", &m_b_akt4_truth_jet_eta); //------Done 
    mytree->Branch ("akt4_truth_jet_phi", &m_b_akt4_truth_jet_phi); //------Done 
    mytree->Branch ("akt4_truth_jet_e",   &m_b_akt4_truth_jet_e); //------Done 

    mytree->Branch("truth_n", &m_b_truth_n, "truth_n/I"); //------Done 
    
    mytree->Branch("truth_charge", &m_b_truth_charge); //------Done 
    mytree->Branch("truth_pt", &m_b_truth_pt); //------Done 
    mytree->Branch("truth_e", &m_b_truth_e); //------Done 
    mytree->Branch("truth_eta", &m_b_truth_eta); //------Done 
    mytree->Branch("truth_phi", &m_b_truth_phi); //------Done 
    mytree->Branch("truth_status", &m_b_truth_status); //------Done 
    mytree->Branch("truth_pdg", &m_b_truth_pdg); //------Done 
    mytree->Branch("truth_type", &m_b_truth_type); //------Done 
    mytree->Branch("truth_origin", &m_b_truth_origin); //------Done 
   
    }
  
  //--------Tracks----------//
  mytree->Branch("ntrk", &m_b_ntrk, "ntrk/I");  //------Done 
  
  mytree->Branch("trk_pt", &m_b_trk_pt); //------Done 
  mytree->Branch("trk_eta", &m_b_trk_eta); //------Done 
  mytree->Branch("trk_phi", &m_b_trk_phi); //------Done 
  mytree->Branch("trk_charge", &m_b_trk_charge); //------Done 
  mytree->Branch("trk_tightPrimary", &m_b_trk_tightPrimary); //------Done 
  mytree->Branch("trk_HILoose", &m_b_trk_HILoose); //------Done 
  mytree->Branch("trk_HITight", &m_b_trk_HITight); //------Done 
  mytree->Branch("trk_d0", &m_b_trk_d0); //------Done 
  mytree->Branch("trk_z0", &m_b_trk_z0); //------Done 
  mytree->Branch("trk_vz", &m_b_trk_vz); //------Done 
  mytree->Branch("trk_theta", &m_b_trk_theta); //------Done 
  mytree->Branch("trk_nPixelHits", &m_b_trk_nPixelHits); //------Done 
  mytree->Branch("trk_nSCTHits", &m_b_trk_nSCTHits); //------Done 
  mytree->Branch("trk_nBlayerHits", &m_b_trk_nBlayerHits); //------Done 
 
  //----------------------------------------------------------------------
  // Initialize jet cleaning tool
  //----------------------------------------------------------------------
  {
    //----Loose Bad
    m_jetCleaningTool = new JetCleaningTool ("JetCleaningTool");
    ANA_CHECK (m_jetCleaningTool->setProperty ("CutLevel","LooseBad"));
    ANA_CHECK (m_jetCleaningTool->setProperty ("DoUgly", false));
    ANA_CHECK (m_jetCleaningTool->initialize ());

    //--Tight Bad
    
    m_jetCleaningTool_TightBad = new JetCleaningTool ("JetCleaningTool");
    ANA_CHECK (m_jetCleaningTool_TightBad->setProperty ("CutLevel","TightBad"));
    ANA_CHECK (m_jetCleaningTool_TightBad->setProperty ("DoUgly", false));
    ANA_CHECK (m_jetCleaningTool_TightBad->initialize ());
    
}



  
  //----------------------------------------------------------------------
  // Track selection tool
  //---------------------------------------------------------------------- 
  m_trackSelectionToolTightPrimary = new InDet::InDetTrackSelectionTool("TrackSelectionToolTightPrimary");
  ANA_CHECK(m_trackSelectionToolTightPrimary->setProperty("CutLevel", "TightPrimary"));
  ANA_CHECK(m_trackSelectionToolTightPrimary->setProperty("minPt", m_trkPtCut*1000.));    
  ANA_CHECK(m_trackSelectionToolTightPrimary->initialize());
  
  m_trackSelectionToolHILoose = new InDet::InDetTrackSelectionTool("TrackSelectionToolHILoose");
  ANA_CHECK(m_trackSelectionToolHILoose->setProperty("CutLevel", "HILoose"));
  ANA_CHECK(m_trackSelectionToolHILoose->setProperty("minPt", m_trkPtCut*1000.));    
  ANA_CHECK(m_trackSelectionToolHILoose->initialize());
  
  m_trackSelectionToolHITight = new InDet::InDetTrackSelectionTool("TrackSelectionToolHITight");
  ANA_CHECK(m_trackSelectionToolHITight->setProperty("CutLevel", "HITight"));
  ANA_CHECK(m_trackSelectionToolHITight->setProperty("minPt", m_trkPtCut*1000.));    
  ANA_CHECK(m_trackSelectionToolHITight->initialize());
  
  
  
  //----------------------------------------------------------------------
  // OOTPU tool
  //----------------------------------------------------------------------
  {
    ANA_MSG_INFO (PathResolverFindCalibFile (Form ("MyJetAnalysis/%s", m_oop_fname.c_str ())).c_str ());
    TFile* f_oop_In = TFile::Open (PathResolverFindCalibFile (Form ("MyJetAnalysis/%s", m_oop_fname.c_str ())).c_str (), "READ");
    
    ANA_MSG_INFO ("Search for pileup file at " << PathResolverFindCalibFile (Form ("MyJetAnalysis/%s", m_oop_fname.c_str ())).c_str ());
    
    if (!f_oop_In) {
      ANA_MSG_ERROR ("Could not find input Out-of-time Pileup calibration file " << m_oop_fname << ", exiting");
      return EL::StatusCode::FAILURE;
    }
    ANA_MSG_INFO ("Read Out-of-time pileup cuts from "<< m_oop_fname);
    m_oop_hMean  = (TH1D*)((TH1D*)f_oop_In->Get("hMeanTotal")) ->Clone("hMeanTotal_HIPileTool");  m_oop_hMean ->SetDirectory(0);
    m_oop_hMean->SetDirectory (0);
    m_oop_hSigma = (TH1D*)((TH1D*)f_oop_In->Get("hSigmaTotal"))->Clone("hSigmaTotal_HIPileTool"); m_oop_hSigma->SetDirectory(0);
    m_oop_hSigma->SetDirectory (0);
  }
  
  // Initialize and configure trigger tools
  if(!mCollissions){
    ANA_CHECK (m_trigConfigTool.initialize());
    ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool
                     
    ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
    ANA_CHECK (m_trigDecisionTool.initialize());
     
    //m_Akt10HI_Insitu_CalibTool = nullptr;
    m_Akt10HI_Insitu_CalibTool = new JetCalibrationTool("Akt10HI_Insitu_CalibTool");
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("JetCollection", "AntiKt10HI"));
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("ConfigFile", "JES_MC16_HI_Jul2019_5TeV.config"));
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("DEVmode", true));
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("CalibSequence", "EtaJES_Insitu"));
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("IsData", true));
    m_Akt10HI_Insitu_CalibTool->initializeTool("Akt10HI_Insitu_CalibTool");

    m_Akt4HI_Insitu_CalibTool = new JetCalibrationTool("Akt4HI_Insitu_CalibTool");
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("JetCollection", "AntiKt10HI"));
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("ConfigFile", "JES_MC16_HI_Jul2019_5TeV.config"));
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("DEVmode", true));
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("CalibSequence", "EtaJES_Insitu"));
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("IsData", true));
    m_Akt4HI_Insitu_CalibTool->initializeTool("Akt4HI_Insitu_CalibTool");

  }else if(mCollissions){
    
    m_Akt10HI_Insitu_CalibTool = new JetCalibrationTool("Akt10HI_Insitu_CalibTool");
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("JetCollection", "AntiKt10HI"));
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("ConfigFile", "JES_MC16_HI_Jul2019_5TeV.config"));
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("DEVmode", true));
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("CalibSequence", "EtaJES_Insitu"));
    ANA_CHECK(m_Akt10HI_Insitu_CalibTool->setProperty("IsData", true));
    m_Akt10HI_Insitu_CalibTool->initializeTool("Akt10HI_Insitu_CalibTool");
  
    m_Akt10HI_EM_EtaJES_CalibTool = nullptr;

    
    m_Akt10HI_EM_EtaJES_CalibTool = new JetCalibrationTool("Akt10HI_EM_EtaJES_CalibTool");
    ANA_CHECK(m_Akt10HI_EM_EtaJES_CalibTool->setProperty("JetCollection", "AntiKt10HI"));
    ANA_CHECK(m_Akt10HI_EM_EtaJES_CalibTool->setProperty("ConfigFile", "JES_MC16_HI_Jul2019_5TeV.config"));
    ANA_CHECK(m_Akt10HI_EM_EtaJES_CalibTool->setProperty("DEVmode", true));
    ANA_CHECK(m_Akt10HI_EM_EtaJES_CalibTool->setProperty("CalibSequence", "EtaJES"));
    ANA_CHECK(m_Akt10HI_EM_EtaJES_CalibTool->setProperty("IsData", mCollissions));
    m_Akt10HI_EM_EtaJES_CalibTool->initializeTool("Akt10HI_EM_EtaJES_CalibTool");
    

    m_Akt4HI_Insitu_CalibTool = new JetCalibrationTool("Akt4HI_Insitu_CalibTool");
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("JetCollection", "AntiKt4HI"));
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("ConfigFile", "JES_MC16_HI_Jul2019_5TeV.config"));
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("DEVmode", true));
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("CalibSequence", "EtaJES_Insitu"));
    ANA_CHECK(m_Akt4HI_Insitu_CalibTool->setProperty("IsData", true));
    m_Akt4HI_Insitu_CalibTool->initializeTool("Akt4HI_Insitu_CalibTool");

    m_Akt4HI_EM_EtaJES_CalibTool = nullptr;


    m_Akt4HI_EM_EtaJES_CalibTool = new JetCalibrationTool("Akt4HI_EM_EtaJES_CalibTool");
    ANA_CHECK(m_Akt4HI_EM_EtaJES_CalibTool->setProperty("JetCollection", "AntiKt4HI"));
    ANA_CHECK(m_Akt4HI_EM_EtaJES_CalibTool->setProperty("ConfigFile", "JES_MC16_HI_Jul2019_5TeV.config"));
    ANA_CHECK(m_Akt4HI_EM_EtaJES_CalibTool->setProperty("DEVmode", true));
    ANA_CHECK(m_Akt4HI_EM_EtaJES_CalibTool->setProperty("CalibSequence", "EtaJES"));
    ANA_CHECK(m_Akt4HI_EM_EtaJES_CalibTool->setProperty("IsData", mCollissions));
    m_Akt4HI_EM_EtaJES_CalibTool->initializeTool("Akt4HI_EM_EtaJES_CalibTool");





    m_truthClassifier = new MCTruthClassifier("mcclasstool");
 }




  return StatusCode::SUCCESS;
}


StatusCode MyJetxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  
  const xAOD::Vertex* priVtx = nullptr;
  //Pb+Pb Triggers
  HLT_j200_a10_ion_L1J50 = false;
  HLT_j180_a10_ion_L1J50 = false;
  HLT_j150_a10_ion_L1J50 = false;

  //pp Triggers
  HLT_j110_a10_lcw_subjes_L1J30 = false;
  HLT_j110 = false;
  //FCal                                                                                                                               
                                                                                                                                       
  m_b_fcalA_et = 0;
  m_b_fcalC_et = 0;


  //Jets R = 0.2, 0.3, 0.4, 1.0                                                                                                        
                                                                                                                                       
  m_b_akt10hi_jet_n = 0;

  m_b_LooseBad_10.clear();
  m_b_TightBad_10.clear();
 

  m_b_akt10hi_constit_jet_pt.clear ();
  m_b_akt10hi_constit_jet_eta.clear ();
  m_b_akt10hi_constit_jet_phi.clear ();
  m_b_akt10hi_constit_jet_e.clear ();

  m_b_akt10hi_em_jet_pt.clear ();
  m_b_akt10hi_em_jet_eta.clear ();
  m_b_akt10hi_em_jet_phi.clear ();
  m_b_akt10hi_em_jet_e.clear ();

  
  m_b_akt10hi_jet_Insitu_calib_pt.clear ();
  m_b_akt10hi_jet_Insitu_calib_eta.clear ();
  m_b_akt10hi_jet_Insitu_calib_phi.clear ();
  m_b_akt10hi_jet_Insitu_calib_e.clear ();

  

  m_b_akt4hi_jet_n = 0;

  m_b_LooseBad_4.clear();
  m_b_TightBad_4.clear();
 

  m_b_akt4hi_constit_jet_pt.clear ();
  m_b_akt4hi_constit_jet_eta.clear ();
  m_b_akt4hi_constit_jet_phi.clear ();
  m_b_akt4hi_constit_jet_e.clear ();

  m_b_akt4hi_em_jet_pt.clear ();
  m_b_akt4hi_em_jet_eta.clear ();
  m_b_akt4hi_em_jet_phi.clear ();
  m_b_akt4hi_em_jet_e.clear ();


  m_b_akt4hi_jet_Insitu_calib_pt.clear ();
  m_b_akt4hi_jet_Insitu_calib_eta.clear ();
  m_b_akt4hi_jet_Insitu_calib_phi.clear ();
  m_b_akt4hi_jet_Insitu_calib_e.clear ();





  //----Truth Jets
  m_b_akt10_truth_jet_n = 0;
  m_b_akt10_truth_jet_pt.clear ();
  m_b_akt10_truth_jet_eta.clear ();
  m_b_akt10_truth_jet_phi.clear ();
  m_b_akt10_truth_jet_e.clear ();

  m_b_akt4_truth_jet_n = 0;
  m_b_akt4_truth_jet_pt.clear ();
  m_b_akt4_truth_jet_eta.clear ();
  m_b_akt4_truth_jet_phi.clear ();
  m_b_akt4_truth_jet_e.clear ();



  //-----Truth Tracks
  m_b_truth_n = 0;
  m_b_truth_charge.clear();
  m_b_truth_pt.clear();
  m_b_truth_e.clear();
  m_b_truth_eta.clear();
  m_b_truth_phi.clear();
  m_b_truth_pdg.clear();
  m_b_truth_status.clear();
  m_b_truth_type.clear();
  m_b_truth_origin.clear();

  //-----Tracks
  m_b_ntrk = 0;
  m_b_trk_pt.clear();
  m_b_trk_eta.clear();
  m_b_trk_phi.clear();
  m_b_trk_charge.clear();
  m_b_trk_tightPrimary.clear();
  m_b_trk_HILoose.clear();
  m_b_trk_HITight.clear();
  m_b_trk_d0.clear();
  m_b_trk_z0.clear();
  m_b_trk_vz.clear();
  m_b_trk_theta.clear();
  m_b_trk_nPixelHits.clear();
  m_b_trk_nSCTHits.clear();
  m_b_trk_nBlayerHits.clear();

  //---Reco Jets
  m_b_akt10_reco_sub_et.clear();
  m_b_akt10_reco_sub_e.clear();
  m_b_akt10_reco_SubET.clear();
  
  m_b_akt10_reco_constit_jet_pt.clear();
  m_b_akt10_reco_constit_jet_eta.clear();
  m_b_akt10_reco_constit_jet_phi.clear();
  m_b_akt10_reco_constit_jet_e.clear();

  m_b_akt10_reco_em_jet_pt.clear();
  m_b_akt10_reco_em_jet_eta.clear();
  m_b_akt10_reco_em_jet_phi.clear();
  m_b_akt10_reco_em_jet_e.clear();

  m_b_akt10_reco_jet_Insitu_calib_pt.clear();
  m_b_akt10_reco_jet_Insitu_calib_eta.clear();
  m_b_akt10_reco_jet_Insitu_calib_phi.clear();
  m_b_akt10_reco_jet_Insitu_calib_e.clear();

  
  m_b_akt10_reco_jet_etaJES_calib_pt.clear();
  m_b_akt10_reco_jet_etaJES_calib_eta.clear();
  m_b_akt10_reco_jet_etaJES_calib_phi.clear();
  m_b_akt10_reco_jet_etaJES_calib_e.clear();


  //-----R=0.4
  m_b_akt4_reco_sub_et.clear();
  m_b_akt4_reco_sub_e.clear();

  m_b_akt4_reco_SubET.clear();

  m_b_akt4_reco_constit_jet_pt.clear();
  m_b_akt4_reco_constit_jet_eta.clear();
  m_b_akt4_reco_constit_jet_phi.clear();
  m_b_akt4_reco_constit_jet_e.clear();

  m_b_akt4_reco_em_jet_pt.clear();
  m_b_akt4_reco_em_jet_eta.clear();
  m_b_akt4_reco_em_jet_phi.clear();
  m_b_akt4_reco_em_jet_e.clear();

  m_b_akt4_reco_jet_Insitu_calib_pt.clear();
  m_b_akt4_reco_jet_Insitu_calib_eta.clear();
  m_b_akt4_reco_jet_Insitu_calib_phi.clear();
  m_b_akt4_reco_jet_Insitu_calib_e.clear();


  m_b_akt4_reco_jet_etaJES_calib_pt.clear();
  m_b_akt4_reco_jet_etaJES_calib_eta.clear();
  m_b_akt4_reco_jet_etaJES_calib_phi.clear();
  m_b_akt4_reco_jet_etaJES_calib_e.clear();



  m_b_akt4_reco_jet_n = 0;
  m_b_akt10_reco_jet_n = 0;
  m_b_akt10_truth_jet_n = 0;
  m_b_akt4_truth_jet_n = 0;
  m_b_nvert = 0;
 
  //Event Number & Run Number                                                                                                        
  m_b_runNumber = eventInfo->runNumber();
  m_b_eventNumber = eventInfo->eventNumber();


  //----------------------------------------------------------------------                                       
  // Calculated total FCal energies                                                                              
  //----------------------------------------------------------------------                                       
  
    const xAOD::HIEventShapeContainer* hiueContainer = 0;
    if (!evtStore ()->retrieve (hiueContainer, "HIEventShape").isSuccess ()) {
      Error ("GetHIEventShape ()", "Failed to retrieve HIEventShape container. Exiting." );
      return EL::StatusCode::FAILURE;
    }
    
    for (const auto* hiue : *hiueContainer) {
      double et = hiue->et ();
      int layer = hiue->layer ();
      double eta = hiue->etaMin ();

      if (layer == 21 || layer == 22 || layer == 23) {
        if (eta > 0)  {
          m_b_fcalA_et += et * 1e-3;
        }
        else {
          m_b_fcalC_et += et * 1e-3;
        }
      }
    }
                                                                                              
    bool lowest_thresh_fire = false;

    if(!mCollissions){
      //----------------------------------------------------------------------                                       
      // Get jets from jet container                                                                                 
      //----------------------------------------------------------------------                                       
      {  
      
      const xAOD::JetContainer* jet10Container = 0;                                                              
      if (!evtStore ()->retrieve (jet10Container, "AntiKt10HIJets").isSuccess ()) {                              
	Error ("GetAntiKt10HIJets ()", "Failed to retrieve AntiKt10HIJets collection. Exiting." );               
	return EL::StatusCode::FAILURE;                                                                          
      }                                                                                                          

      for (const auto* init_jet : *jet10Container) {                                                             
     
	xAOD::Jet* jet_1 = new xAOD::Jet();
	jet_1->makePrivateStore(*init_jet);

	// Jet quality check
	const bool isLooseBad_10 = m_jetCleaningTool->keep (*jet_1); // implements jet cleaning at the "LooseBad" selection level
	const bool isTightBad_10 = m_jetCleaningTool_TightBad->keep (*jet_1);
	
	const xAOD::JetFourMom_t jet_4mom_constit = init_jet->jetP4("JetConstitScaleMomentum");
	const float constit_pt = 1e-3 * jet_4mom_constit.pt();
	const float constit_eta = jet_4mom_constit.eta();
	const float constit_phi = jet_4mom_constit.phi();
	const float constit_e = 1e-3*jet_4mom_constit.e();


	
	//// for derivations, only unsubtracted and subtracted momenta are stored, so we need to set the constituent and EM scales(which are the same, respectively)
	jet_1->setJetP4("JetConstitScaleMomentum", jet_1->jetP4("JetUnsubtractedScaleMomentum"));
	jet_1->setJetP4("JetEMScaleMomentum", jet_1->jetP4("JetSubtractedScaleMomentum"));

	//// set pre-EtaJES 4-momentum at the EM scale
	const xAOD::JetFourMom_t jet_4mom_em = jet_1->jetP4("JetEMScaleMomentum");
	jet_1->setJetP4("JetPileupScaleMomentum", jet_4mom_em);

	const float em_pt = 1e-3 * jet_4mom_em.pt();
	const float em_eta = jet_4mom_em.eta();
	const float em_phi = jet_4mom_em.phi();
	const float em_e = 1e-3 * jet_4mom_em.e();
	
	ANA_CHECK(m_Akt10HI_Insitu_CalibTool->applyCalibration(*jet_1));
	const xAOD::JetFourMom_t jet_4mom_insitu = jet_1->jetP4();
	const float xcalib_pt_insitu = 1e-3 * jet_4mom_insitu.pt ();
	const float xcalib_eta_insitu = jet_4mom_insitu.eta ();
	const float xcalib_phi_insitu = jet_4mom_insitu.phi ();
	const float xcalib_e_insitu = 1e-3 * jet_4mom_insitu.e ();
	
	
	//std::cout << "This the pt of your jet! " << xcalib_pt << std::endl;
	// only write jets above jets pT cut                                                                     
	if (xcalib_pt_insitu  < 100) {                                                                          
	  delete jet_1;
	  continue;                                                                                              
	}                                                                                                        
	m_b_LooseBad_10.push_back(isLooseBad_10);
	m_b_TightBad_10.push_back(isTightBad_10);
	

	m_b_akt10hi_constit_jet_pt.push_back (constit_pt);                                                                
	m_b_akt10hi_constit_jet_eta.push_back (constit_eta);                                                              
	m_b_akt10hi_constit_jet_phi.push_back (constit_phi);                         
	m_b_akt10hi_constit_jet_e.push_back (constit_e);                                                                  
	
	m_b_akt10hi_em_jet_pt.push_back (em_pt);
	m_b_akt10hi_em_jet_eta.push_back (em_eta);
	m_b_akt10hi_em_jet_phi.push_back (em_phi);
	m_b_akt10hi_em_jet_e.push_back (em_e);

	m_b_akt10hi_jet_Insitu_calib_pt.push_back (xcalib_pt_insitu);
	m_b_akt10hi_jet_Insitu_calib_eta.push_back (xcalib_eta_insitu);
	m_b_akt10hi_jet_Insitu_calib_phi.push_back (xcalib_phi_insitu);
	m_b_akt10hi_jet_Insitu_calib_e.push_back (xcalib_e_insitu);	


	m_b_akt10hi_jet_n++;               
	
	if (jet_1) delete jet_1;
      }// end jet loop                                                                                           
      }
      {
	
	
	const xAOD::JetContainer* jet4Container = 0;
	if (!evtStore ()->retrieve (jet4Container, "AntiKt4HIJets").isSuccess ()) {
	  Error ("GetAntiKt4HIJets ()", "Failed to retrieve AntiKt4HIJets collection. Exiting." );
	  return EL::StatusCode::FAILURE;
	}
	
	for (const auto* init_jet : *jet4Container) {
	  
	  xAOD::Jet* jet_1 = new xAOD::Jet();
	  jet_1->makePrivateStore(*init_jet);

	 
	  // Jet quality check                                                                                                                                                             
	  const bool isLooseBad_4 = m_jetCleaningTool->keep (*jet_1); // implements jet cleaning at the "LooseBad" selection level                                                      
	  const bool isTightBad_4 = m_jetCleaningTool_TightBad->keep (*jet_1);




	  const xAOD::JetFourMom_t jet_4mom_constit = init_jet->jetP4("JetConstitScaleMomentum");
	  const float constit_pt = 1e-3 * jet_4mom_constit.pt();
	  const float constit_eta = jet_4mom_constit.eta();
	  const float constit_phi = jet_4mom_constit.phi();
	  const float constit_e = 1e-3*jet_4mom_constit.e();

	  //// for derivations, only unsubtracted and subtracted momenta are stored, so we need to set the constituent and EM scales(which are the same, respectively)                                        
	  jet_1->setJetP4("JetConstitScaleMomentum", jet_1->jetP4("JetUnsubtractedScaleMomentum"));
	  jet_1->setJetP4("JetEMScaleMomentum", jet_1->jetP4("JetSubtractedScaleMomentum"));

	  //// set pre-EtaJES 4-momentum at the EM scale                                                                                                                                                      
	  const xAOD::JetFourMom_t jet_4mom_em = jet_1->jetP4("JetEMScaleMomentum");
	  jet_1->setJetP4("JetPileupScaleMomentum", jet_4mom_em);

	  const float em_pt = 1e-3 * jet_4mom_em.pt();
	  const float em_eta = jet_4mom_em.eta();
	  const float em_phi = jet_4mom_em.phi();
	  const float em_e = 1e-3 * jet_4mom_em.e();
	  
	  ANA_CHECK(m_Akt4HI_Insitu_CalibTool->applyCalibration(*jet_1));
	  const xAOD::JetFourMom_t jet_4mom_insitu = jet_1->jetP4();
	  const float xcalib_pt_insitu = 1e-3 * jet_4mom_insitu.pt ();
	  const float xcalib_eta_insitu = jet_4mom_insitu.eta ();
	  const float xcalib_phi_insitu = jet_4mom_insitu.phi ();
	  const float xcalib_e_insitu = 1e-3 * jet_4mom_insitu.e ();

	  
	  //std::cout << "This the pt of your jet! " << xcalib_pt << std::endl;                                                                                                                               
	  // only write jets above jets pT cut                                                                                                                                                                
	  if (xcalib_pt_insitu  < 100) {
	    delete jet_1;
	    continue;
	  }

	  
	  m_b_LooseBad_4.push_back(isLooseBad_4);
	  m_b_TightBad_4.push_back(isTightBad_4);
	 
	  m_b_akt4hi_constit_jet_pt.push_back (constit_pt);
	  m_b_akt4hi_constit_jet_eta.push_back (constit_eta);
	  m_b_akt4hi_constit_jet_phi.push_back (constit_phi);
	  m_b_akt4hi_constit_jet_e.push_back (constit_e);
	  
	  m_b_akt4hi_em_jet_pt.push_back (em_pt);
	  m_b_akt4hi_em_jet_eta.push_back (em_eta);
	  m_b_akt4hi_em_jet_phi.push_back (em_phi);
	  m_b_akt4hi_em_jet_e.push_back (em_e);

	  m_b_akt4hi_jet_Insitu_calib_pt.push_back (xcalib_pt_insitu);
	  m_b_akt4hi_jet_Insitu_calib_eta.push_back (xcalib_eta_insitu);
	  m_b_akt4hi_jet_Insitu_calib_phi.push_back (xcalib_phi_insitu);
	  m_b_akt4hi_jet_Insitu_calib_e.push_back (xcalib_e_insitu);

	  
	  m_b_akt4hi_jet_n++;

	  if (jet_1) delete jet_1;
	}// end jet loop
      }
  
      //Triggers
      
      if(!mCollissions){
	{
	  auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_j200_a10_ion_L1J50");
	  HLT_j200_a10_ion_L1J50 = chainGroup->isPassed();
	}
	{
	  auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_j180_a10_ion_L1J50");
	  HLT_j180_a10_ion_L1J50 = chainGroup->isPassed();
	  //lowest_thresh_fire = HLT_j180_a10_ion_L1J50; Comment out when looking at Pb+Pb data
	}
	{
	  auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_j110_a10_lcw_subjes_L1J30");
	  HLT_j110_a10_lcw_subjes_L1J30 = chainGroup->isPassed();
	  lowest_thresh_fire = HLT_j110_a10_lcw_subjes_L1J30;
	}
	{
	  auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_j110");
          HLT_j110 = chainGroup->isPassed();
          
	}
	{
	  auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_j150_a10_ion_L1J50");
	  HLT_j150_a10_ion_L1J50 = chainGroup->isPassed();
	}
      }
      //if(lowest_thresh_fire==1){tree ("analysis")->Fill ();} 
    }

    
    //----------------------------------------------------------------------                
    // Get tracking info                                                                    
    //----------------------------------------------------------------------                
    if(track_bool) {
      const xAOD::TrackParticleContainer* trackContainer = 0;
      if(!evtStore()->retrieve(trackContainer, "InDetTrackParticles").isSuccess()) {
	Error("GetInDetTrackParticles()", "Failed to retrieve InDetTrackParticles container. Exiting.");
	return EL::StatusCode::FAILURE;
      }
    
      for(const auto* track : *trackContainer) {
	if(track->pt()*1e-3 < m_trkPtCut)
	  continue;
	
	//m_track_hist->Fill(0);
	
	const bool passTightPrimary = m_trackSelectionToolTightPrimary->accept(*track);
	const bool passHILoose = m_trackSelectionToolHILoose->accept(*track, priVtx);
	const bool passHITight = m_trackSelectionToolHITight->accept(*track, priVtx);
	
	if(!passTightPrimary && !passHILoose && !passHITight) continue;

	//m_track_hist->Fill(1);
	
	m_b_trk_pt.push_back(track->pt()*1e-3);

	
	m_b_trk_eta.push_back(track->eta());
	m_b_trk_phi.push_back(track->phi());
	m_b_trk_charge.push_back(track->charge());
	
	m_b_trk_tightPrimary.push_back(passTightPrimary);
	m_b_trk_HILoose.push_back(passHILoose);
	m_b_trk_HITight.push_back(passHITight);

	m_b_trk_d0.push_back(track->d0());
	m_b_trk_z0.push_back(track->z0());
	m_b_trk_vz.push_back(track->vz());
	m_b_trk_theta.push_back(track->theta());

	m_b_trk_nBlayerHits.push_back(getSum(*track, xAOD::numberOfBLayerHits));
	m_b_trk_nPixelHits.push_back(getSum(*track, xAOD::numberOfPixelHits) + getSum(*track, xAOD::numberOfPixelDeadSensors));
	m_b_trk_nSCTHits.push_back(getSum(*track, xAOD::numberOfSCTHits) + getSum(*track, xAOD::numberOfSCTDeadSensors));

	m_b_ntrk++;
      } // end tracks loop                                                                  
    }


    



   //----------------------------------------------------------------------      
   // Get Reco jets if MC                                                                       
   //----------------------------------------------------------------------                                                              
  if (mCollissions) {
    {
    //----Reco Jets
    // Use a TStore to avoid memory leaks- see documentation on cross-calibration for details
    xAOD::TStore* store = new xAOD::TStore;
    xAOD::JetContainer* jets4_calibrated = new xAOD::JetContainer();
    xAOD::AuxContainerBase* jets4_aux = new xAOD::AuxContainerBase();
    jets4_calibrated->setStore(jets4_aux);
    store->record(jets4_calibrated, "jets4_calibrated");
    store->record(jets4_aux, "jets4_aux");

    const xAOD::JetContainer* jetContainer = 0;
    if(!evtStore()->retrieve(jetContainer, "AntiKt10HIJets").isSuccess()) {
      Error("GetAntiKt10HIJets()", "Failed to retrieve AntiKt4HIJets collection. Exiting." );
      return EL::StatusCode::FAILURE;
    }
    
    
    for(const auto* init_jet : *jetContainer){
      //opting instead to keep the result of the cleaning as a boolean
      //      if(!m_jetCleaningTool->keep(*init_jet)) continue; // implements jet cleaning by rejecting "dirty" jets
    
      xAOD::Jet* jet = new xAOD::Jet();
      jet->makePrivateStore(*init_jet);

      // HI jet subtracted energy                                                                                                                                                                                              
                                                                                                                                                                                                             
      const xAOD::JetFourMom_t sub_4mom = -init_jet->jetP4 ("JetSubtractedScaleMomentum") + init_jet->jetP4 ("JetUnsubtractedScaleMomentum");
      const float sub_et = sub_4mom.Et () * 1e-3;
      const float sub_e = sub_4mom.E () * 1e-3;
      
      const float ET_before_sub  = init_jet->jetP4 ("JetUnsubtractedScaleMomentum").Et() * 1e-3;
      const float ET_after_sub  = init_jet->jetP4 ("JetSubtractedScaleMomentum").Et() * 1e-3;
      const float subtracted_ET = ET_before_sub - ET_after_sub;

      
      const xAOD::JetFourMom_t jet_4mom_constit = init_jet->jetP4("JetConstitScaleMomentum");
      const float constit_pt = 1e-3 * jet_4mom_constit.pt();
      const float constit_eta = jet_4mom_constit.eta();
      const float constit_phi = jet_4mom_constit.phi();
      const float constit_e = 1e-3*jet_4mom_constit.e();

      
      
      //// for derivations, only unsubtracted and subtracted momenta are stored, so we need to set the constituent and EM scales(which are the same, respectively)
      jet->setJetP4("JetConstitScaleMomentum", jet->jetP4("JetUnsubtractedScaleMomentum"));
      jet->setJetP4("JetEMScaleMomentum", jet->jetP4("JetSubtractedScaleMomentum"));

      //// set pre-EtaJES 4-momentum at the EM scale
      const xAOD::JetFourMom_t jet_4mom_em = jet->jetP4("JetEMScaleMomentum");
      jet->setJetP4("JetPileupScaleMomentum", jet_4mom_em);

      const float em_pt = 1e-3*jet_4mom_em.pt();
      const float em_eta = jet_4mom_em.eta();
      const float em_phi = jet_4mom_em.phi();
      const float em_e = 1e-3*jet_4mom_em.e();
      
      //// apply EtaJES calibration
      ANA_CHECK(m_Akt10HI_EM_EtaJES_CalibTool->applyCalibration(*jet));

      //// set pre-insitu correction 4-momentum
      const xAOD::JetFourMom_t jet_4mom_etajes = jet->jetP4();
      const float etajes_pt = 1e-3*jet_4mom_etajes.pt();
      const float etajes_eta = jet_4mom_etajes.eta();
      const float etajes_phi = jet_4mom_etajes.phi();
      const float etajes_e = 1e-3*jet_4mom_etajes.e();
      
      //---Insitu Corrections
      ANA_CHECK(m_Akt10HI_Insitu_CalibTool->applyCalibration(*jet));
      const xAOD::JetFourMom_t jet_4mom_xcalib = jet->jetP4();
      
      const float xcalib_pt_insitu = 1e-3*jet_4mom_xcalib.pt();
      const float xcalib_eta_insitu = jet_4mom_xcalib.eta();
      const float xcalib_phi_insitu = jet_4mom_xcalib.phi();
      const float xcalib_e_insitu = 1e-3*jet_4mom_xcalib.e();

      if (xcalib_pt_insitu < 100){
	 
	if (jet) delete jet;
	continue; // truth jet pT cut 
      }
      
      m_b_akt10_reco_sub_et.push_back(sub_et);
      m_b_akt10_reco_sub_e.push_back(sub_e);
      m_b_akt10_reco_SubET.push_back(subtracted_ET);

      m_b_akt10_reco_jet_etaJES_calib_pt.push_back (etajes_pt);
      m_b_akt10_reco_jet_etaJES_calib_eta.push_back (etajes_eta);
      m_b_akt10_reco_jet_etaJES_calib_phi.push_back (etajes_phi);
      m_b_akt10_reco_jet_etaJES_calib_e.push_back (etajes_e);
      
      m_b_akt10_reco_constit_jet_pt.push_back (constit_pt);
      m_b_akt10_reco_constit_jet_eta.push_back (constit_eta);
      m_b_akt10_reco_constit_jet_phi.push_back (constit_phi);
      m_b_akt10_reco_constit_jet_e.push_back (constit_e);

      m_b_akt10_reco_em_jet_pt.push_back (em_pt);
      m_b_akt10_reco_em_jet_eta.push_back (em_eta);
      m_b_akt10_reco_em_jet_phi.push_back (em_phi);
      m_b_akt10_reco_em_jet_e.push_back (em_e);

      m_b_akt10_reco_jet_Insitu_calib_pt.push_back (xcalib_pt_insitu);
      m_b_akt10_reco_jet_Insitu_calib_eta.push_back (xcalib_eta_insitu);
      m_b_akt10_reco_jet_Insitu_calib_phi.push_back (xcalib_phi_insitu);
      m_b_akt10_reco_jet_Insitu_calib_e.push_back (xcalib_e_insitu);
      
      m_b_akt10_reco_jet_n++;
      
      if (jet) delete jet;
      
    }
    
    }//End of Scope for R=1.0 Jets

    {
      //----Reco Jets                                                                                                                                                                                      
      // Use a TStore to avoid memory leaks- see documentation on cross-calibration for details                                                                                                            
      xAOD::TStore* store = new xAOD::TStore;
      xAOD::JetContainer* jets4_calibrated = new xAOD::JetContainer();
      xAOD::AuxContainerBase* jets4_aux = new xAOD::AuxContainerBase();
      jets4_calibrated->setStore(jets4_aux);
      store->record(jets4_calibrated, "jets4_calibrated");
      store->record(jets4_aux, "jets4_aux");

      const xAOD::JetContainer* jetContainer = 0;
      if(!evtStore()->retrieve(jetContainer, "AntiKt4HIJets").isSuccess()) {
	Error("GetAntiKt4HIJets()", "Failed to retrieve AntiKt4HIJets collection. Exiting." );
	return EL::StatusCode::FAILURE;
      }

      for(const auto* init_jet : *jetContainer){
	//opting instead to keep the result of the cleaning as a boolean                                                                                                                                   
	                     

	xAOD::Jet* jet = new xAOD::Jet();
	jet->makePrivateStore(*init_jet);


	// HI jet subtracted energy                                                                                                                                                                                                 

        const xAOD::JetFourMom_t sub_4mom = -init_jet->jetP4 ("JetSubtractedScaleMomentum") + init_jet->jetP4 ("JetUnsubtractedScaleMomentum");
        const float sub_et = sub_4mom.Et () * 1e-3;
        const float sub_e = sub_4mom.E () * 1e-3;

	
	const float ET_before_sub  = init_jet->jetP4 ("JetUnsubtractedScaleMomentum").Et() * 1e-3;
	const float ET_after_sub  = init_jet->jetP4 ("JetSubtractedScaleMomentum").Et() * 1e-3;
	const float subtracted_ET = ET_before_sub - ET_after_sub;
        
	

	const xAOD::JetFourMom_t jet_4mom_constit = init_jet->jetP4("JetConstitScaleMomentum");
	const float constit_pt = 1e-3 * jet_4mom_constit.pt();
	const float constit_eta = jet_4mom_constit.eta();
	const float constit_phi = jet_4mom_constit.phi();
	const float constit_e = 1e-3*jet_4mom_constit.e();


	//// for derivations, only unsubtracted and subtracted momenta are stored, so we need to set the constituent and EM scales(which are the same, respectively)                                      
	jet->setJetP4("JetConstitScaleMomentum", jet->jetP4("JetUnsubtractedScaleMomentum"));
	jet->setJetP4("JetEMScaleMomentum", jet->jetP4("JetSubtractedScaleMomentum"));

	//// set pre-EtaJES 4-momentum at the EM scale                                                                                                                                                    
	const xAOD::JetFourMom_t jet_4mom_em = jet->jetP4("JetEMScaleMomentum");
	jet->setJetP4("JetPileupScaleMomentum", jet_4mom_em);

	const float em_pt = 1e-3*jet_4mom_em.pt();
	const float em_eta = jet_4mom_em.eta();
	const float em_phi = jet_4mom_em.phi();
	const float em_e = 1e-3*jet_4mom_em.e();

	//// apply EtaJES calibration                                                                                                                                                                         
	ANA_CHECK(m_Akt4HI_EM_EtaJES_CalibTool->applyCalibration(*jet));


	//// set pre-insitu correction 4-momentum                                                                                                                      
	const xAOD::JetFourMom_t jet_4mom_etajes = jet->jetP4();
	const float etajes_pt = 1e-3*jet_4mom_etajes.pt();
	const float etajes_eta = jet_4mom_etajes.eta();
	const float etajes_phi = jet_4mom_etajes.phi();
	const float etajes_e = 1e-3*jet_4mom_etajes.e();

	//---Insitu Corrections                                                                                                                                        
	ANA_CHECK(m_Akt10HI_Insitu_CalibTool->applyCalibration(*jet));
	const xAOD::JetFourMom_t jet_4mom_xcalib = jet->jetP4();

	
	const float xcalib_pt_insitu = 1e-3*jet_4mom_xcalib.pt();
	const float xcalib_eta_insitu = jet_4mom_xcalib.eta();
	const float xcalib_phi_insitu = jet_4mom_xcalib.phi();
	const float xcalib_e_insitu = 1e-3*jet_4mom_xcalib.e();

	if (xcalib_pt_insitu < 100){

	  if (jet) delete jet;
	  continue; // truth jet pT cut                                                                                                                                                                       
	}

	m_b_akt4_reco_sub_et.push_back(sub_et);
	m_b_akt4_reco_sub_e.push_back(sub_e);
	m_b_akt4_reco_SubET.push_back(subtracted_ET);

	m_b_akt4_reco_jet_etaJES_calib_pt.push_back (etajes_pt);
	m_b_akt4_reco_jet_etaJES_calib_eta.push_back (etajes_eta);
	m_b_akt4_reco_jet_etaJES_calib_phi.push_back (etajes_phi);
	m_b_akt4_reco_jet_etaJES_calib_e.push_back (etajes_e);

	m_b_akt4_reco_constit_jet_pt.push_back (constit_pt);
	m_b_akt4_reco_constit_jet_eta.push_back (constit_eta);
	m_b_akt4_reco_constit_jet_phi.push_back (constit_phi);
	m_b_akt4_reco_constit_jet_e.push_back (constit_e);

	m_b_akt4_reco_em_jet_pt.push_back (em_pt);
	m_b_akt4_reco_em_jet_eta.push_back (em_eta);
	m_b_akt4_reco_em_jet_phi.push_back (em_phi);
	m_b_akt4_reco_em_jet_e.push_back (em_e);

	m_b_akt4_reco_jet_Insitu_calib_pt.push_back (xcalib_pt_insitu);
	m_b_akt4_reco_jet_Insitu_calib_eta.push_back (xcalib_eta_insitu);
	m_b_akt4_reco_jet_Insitu_calib_phi.push_back (xcalib_phi_insitu);
	m_b_akt4_reco_jet_Insitu_calib_e.push_back (xcalib_e_insitu);

	m_b_akt4_reco_jet_n++;

	if (jet) delete jet;

      }




    }//End of Scope for R=0.4 Jets



  }





  //----------------------------------------------------------------------
  //----------------------------------------------------------------------
  // Get truth jets if MC
  //----------------------------------------------------------------------
  if(mCollissions){
    //-------------------------------//
    //----------R = 1.0 Jets---------//
    //-------------------------------//
      const xAOD::JetContainer* truthJetContainer = 0;
      if(!evtStore()->retrieve(truthJetContainer, "AntiKt10TruthJets").isSuccess()) {
	Error("GetAntiKt10TruthJets()", "Failed to retrieve AntiKt10TruthJets collection. Exiting.");
	return EL::StatusCode::FAILURE;
      }
      
      for(const auto* truthJet : *truthJetContainer) {
	if(truthJet->pt()*1e-3 < 100)
	  continue; // truth jet pT cut
	
	m_b_akt10_truth_jet_pt.push_back(truthJet->pt()*1e-3);
	m_b_akt10_truth_jet_eta.push_back(truthJet->eta());
	m_b_akt10_truth_jet_phi.push_back(truthJet->phi());
	m_b_akt10_truth_jet_e.push_back(truthJet->e()*1e-3);
	
	m_b_akt10_truth_jet_n++;
      } // end truth jet loop
      {
      //-------------------------------//                                                                                                                                                                 
      //----------R = 0.4 Jets---------//                                                                                                                                                               
      //-------------------------------//                                                                                                                                                                 

      const xAOD::JetContainer* truthJetContainer = 0;
      if(!evtStore()->retrieve(truthJetContainer, "AntiKt4TruthJets").isSuccess()) {
        Error("GetAntiKt4TruthJets()", "Failed to retrieve AntiKt4TruthJets collection. Exiting.");
        return EL::StatusCode::FAILURE;
      }

      for(const auto* truthJet : *truthJetContainer) {
        if(truthJet->pt()*1e-3 < 100)
          continue; // truth jet pT cut                                                                                                                                                                   
                                                                                                                                                                                                   
        m_b_akt4_truth_jet_pt.push_back(truthJet->pt()*1e-3);
        m_b_akt4_truth_jet_eta.push_back(truthJet->eta());
        m_b_akt4_truth_jet_phi.push_back(truthJet->phi());
        m_b_akt4_truth_jet_e.push_back(truthJet->e()*1e-3);

        m_b_akt4_truth_jet_n++;
      } // end truth jet loop                                                                                                                                                                            
      }                                                                                                                                                                                                  






  }


  
  //----------------------------------------------------------------------
  // Get truth tracks if MC
  //----------------------------------------------------------------------
  {
    if(mCollissions){
      const xAOD::TruthParticleContainer* truthParticleContainer = 0;
      if(!evtStore()->retrieve(truthParticleContainer, "TruthParticles").isSuccess()){
	Error("GetTruthParticles()", "Failed to retrieve TruthParticles container. Exiting.");
	return EL::StatusCode::FAILURE;
      }
      
      unsigned int pos = 0;
      std::vector<TLorentzVector> hardScattParticles;
      std::vector<int> pdgId;
      for(const auto & truthParticle : *truthParticleContainer){
	++pos;

	if(truthParticle->pt() < 5.0*1e-3) continue;

	hardScattParticles.push_back(TLorentzVector(truthParticle->px(), truthParticle->py(), truthParticle->pz(), truthParticle->e()));
	pdgId.push_back(truthParticle->pdgId());

	if(pos >= 10) break;
      }

      //Find pthat
      if(hardScattParticles.size() > 0){
	m_b_pthat = 0.0;

	for(unsigned int pI = 0; pI < hardScattParticles.size()-1; ++pI){
	  if(hardScattParticles[pI].Pt() < 5.0*1e-3) continue;
	  
	  for(unsigned int pI2 = pI+1; pI2 < hardScattParticles.size(); ++pI2){
	    if(hardScattParticles[pI2].Pt() < 5.0*1e-3) continue;
	    
	    if(TMath::Abs(hardScattParticles[pI].Px() + hardScattParticles[pI2].Px()) > 1.0) continue;
	    if(TMath::Abs(hardScattParticles[pI].Py() + hardScattParticles[pI2].Py()) > 1.0) continue;
	    //if(TMath::Abs(hardScattParticles[pI].Pz() + hardScattParticles[pI2].Pz()) > 1.0) continue;Lol duh this condition does not haveto be satisfied
	    
	    m_b_treePartonPt[0] = hardScattParticles[pI].Pt()*1e-3;
	    m_b_treePartonEta[0] = hardScattParticles[pI].Eta();
	    m_b_treePartonPhi[0] = hardScattParticles[pI].Phi();
	    m_b_treePartonId[0] = pdgId[pI];

	    m_b_treePartonPt[1] = hardScattParticles[pI2].Pt()*1e-3;
	    m_b_treePartonEta[1] = hardScattParticles[pI2].Eta();
	    m_b_treePartonPhi[1] = hardScattParticles[pI2].Phi();
	    m_b_treePartonId[1] = pdgId[pI2];
	    
	    m_b_pthat = m_b_treePartonPt[0];
	    break;
	  }
	  
	  if(m_b_pthat > 1) break;
	}
	
	hardScattParticles.clear();
	pdgId.clear();
      }
      else{
	m_b_pthat = 0.0;
	
	for(Int_t pI = 0; pI < 2; ++pI){
          m_b_treePartonPt[pI] = 0.0;
          m_b_treePartonEta[pI] = -999;
          m_b_treePartonPhi[pI] = -999;
          m_b_treePartonId[pI] = 0;
	}
      }
      
      std::pair<ParticleType,ParticleOrigin> res;
      for(const auto* truthParticle : *truthParticleContainer){
	//      We will keep intermediates
	//if(truthParticle->status() != 1) continue; // if not final state continue
       
	if(truthParticle->pt()*1e-3 < m_truthTrkPtCut)
	  continue; // pT cut
	//if(!truthParticle->isCharged()) continue; // require charged particles
	if(fabs(truthParticle->eta()) > 3)
	  continue; // require particles inside tracker
	
	//if(truthParticle->absPdgId() == 12 || truthParticle->absPdgId() == 14 || truthParticle->absPdgId() == 16) continue; // don't count neutrinos
	
	if(truthParticle->barcode() >= 2e5 || truthParticle->barcode() == 0) continue;
	
	res = m_truthClassifier->particleTruthClassifier(truthParticle);
	
	//ATH_MSG_INFO("RES OUT: " << res.first << ", " << res.second);

	m_b_truth_pt.push_back(truthParticle->pt()*1e-3);
	m_b_truth_e.push_back(truthParticle->e()*1e-3);
	m_b_truth_eta.push_back(truthParticle->eta());
	m_b_truth_phi.push_back(truthParticle->phi());
	m_b_truth_charge.push_back(truthParticle->charge());
	m_b_truth_pdg.push_back(truthParticle->pdgId());
	m_b_truth_status.push_back(truthParticle->status());
	m_b_truth_type.push_back(res.first);
	m_b_truth_origin.push_back(res.second);
	m_b_truth_n++;
	
      }

    } // end truth particles loop
  } // end tracks scope                

 


  //----------------------------------------------------------------------
  // Get vertices information
  //----------------------------------------------------------------------
  {
   
    m_b_nvert = 0;
    
    const xAOD::VertexContainer* primaryVertices = 0;
    if (!evtStore ()->retrieve (primaryVertices, "PrimaryVertices") .isSuccess ())  {
      Error ("GetPrimaryVertices ()", "Failed to retrieve PrimaryVertices container. Exiting.");
      return EL::StatusCode::FAILURE;
    }
    
    for (const auto* vert : *primaryVertices) {
      if (m_b_nvert >= m_max_nvert) {
        Error ("GetPrimaryVertices ()", "Tried to overflow vertex arrays. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      /**
      if (m_b_nvert == 0) {
        std::cout << "vertex type is ";
        switch (vert->vertexType ()) {
          case xAOD::VxType::NoVtx:         std::cout << "NoVtx";        break;
          case xAOD::VxType::PriVtx:        std::cout << "PriVtx";       break;
          case xAOD::VxType::SecVtx:        std::cout << "SecVtx";       break;
          case xAOD::VxType::PileUp:        std::cout << "PileUp";       break;
          case xAOD::VxType::ConvVtx:       std::cout << "ConvVtx";      break;
          case xAOD::VxType::V0Vtx:         std::cout << "V0Vtx";        break;
          case xAOD::VxType::KinkVtx:       std::cout << "KinkVtx";      break;
          case xAOD::VxType::NotSpecified:  std::cout << "NotSpecified"; break;
        }
        std::cout << std::endl;
	}*/
      
      if (m_b_nvert == 0 && vert){
        priVtx = vert;
      }
      m_b_vert_x[m_b_nvert] = vert->x ();
      m_b_vert_y[m_b_nvert] = vert->y ();
      m_b_vert_z[m_b_nvert] = vert->z ();
      m_b_vert_ntrk[m_b_nvert] = vert->nTrackParticles ();
      m_b_vert_type[m_b_nvert] = vert->vertexType ();
      
     
      float sumpt = 0;
      
      //std::cout << "vert->trackParticleLinks ()->size () = " << vert->trackParticleLinks ()->size () << std::endl;      
      for (const auto track : vert->trackParticleLinks ()) {
	
	if (!track.isValid ()){
          continue;
	}
	
        if (!m_trackSelectionToolHILoose->accept (**track, vert)){
	  
	  continue;
	}
	
        sumpt += (*track)->pt () * 1e-3;
      } // end loop over tracks
      m_b_vert_sumpt[m_b_nvert] = sumpt;
      m_b_nvert++;
    }
    if (m_b_nvert == 0 || priVtx == nullptr) {
      Info ("GetPrimaryVertices ()", "Warning: Did not find a primary vertex!");
      return EL::StatusCode::SUCCESS;

    }else assert (priVtx->vertexType () == xAOD::VxType::PriVtx);

    
  } // end vertex scope




  if(mCollissions){

  //----------------------------------------------------------------------
  // Check for out of time pile up (OOTPU)
  //----------------------------------------------------------------------
  {
    
    if (m_collisionType == "PbPb18") {
      const xAOD::TrackParticleContainer* trackContainer = 0;
      if (!evtStore ()->retrieve (trackContainer, "InDetTrackParticles").isSuccess ()) {
        Error ("GetOutOfTimePileUp ()", "Failed to retrieve InDetTrackParticles container. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      
      const xAOD::HIEventShapeContainer* caloSums = 0;  
      if (!evtStore ()->retrieve (caloSums, "CaloSums").isSuccess ()) {
        Error ("GetOutOfTimePileUp ()", "Failed to retrieve CaloSums container. Exiting.");
        return EL::StatusCode::FAILURE;
      }
      
      int nTracks = 0;
      for (const auto* track : *trackContainer) {
        if (track->pt () * 1e-3 < 0.5) // cut at 500 MeV
          continue;
        if (m_trackSelectionToolHITight->accept (*track, priVtx))
          nTracks++;
      } 
      
      m_b_isOOTPU = is_Outpileup (*caloSums, nTracks);
    }
    else {
      m_b_isOOTPU = false;
      
    }

  } // end OOTPU scope
  }
  
  if(!mCollissions){
    
    //if(HLT_j150_a10_ion_L1J50 || HLT_j180_a10_ion_L1J50){mytree->Fill();}
    if(HLT_j110_a10_lcw_subjes_L1J30 || HLT_j110){mytree->Fill();} 
  }else {
    mytree->Fill(); 
    
  }
  return StatusCode::SUCCESS;
}



StatusCode MyJetxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  
  // Delete jet cleaning tool
  if (m_jetCleaningTool) {
    delete m_jetCleaningTool;
    m_jetCleaningTool = nullptr;
  }

 
  if (m_jetCleaningTool_TightBad) {
    delete m_jetCleaningTool_TightBad;
    m_jetCleaningTool_TightBad = nullptr;
  }

 



  // Delete out-of-time pileup histograms
  if (m_oop_hMean) {
    delete m_oop_hMean;
    m_oop_hMean = nullptr;
  }
  if (m_oop_hSigma) {
    delete m_oop_hSigma;
    m_oop_hSigma = nullptr;
  }

  if (m_trackSelectionToolHITight) {
    delete m_trackSelectionToolHITight;
    m_trackSelectionToolHITight = nullptr;
  }
  if (m_trackSelectionToolHILoose) {
    delete m_trackSelectionToolHILoose;
    m_trackSelectionToolHILoose = nullptr;
  }
  // Delete jet calibration tools

  if(mCollissions){
    if (m_Akt10HI_EM_EtaJES_CalibTool) {
      delete m_Akt10HI_EM_EtaJES_CalibTool;
      m_Akt10HI_EM_EtaJES_CalibTool = nullptr;
    }
    if (m_Akt4HI_EM_EtaJES_CalibTool) {
      delete m_Akt4HI_EM_EtaJES_CalibTool;
      m_Akt4HI_EM_EtaJES_CalibTool = nullptr;
    }

  }
  
  if (m_collisionType == "PbPb18" && m_Akt10HI_Insitu_CalibTool) {
    delete m_Akt10HI_Insitu_CalibTool;
    m_Akt10HI_Insitu_CalibTool = nullptr;
  }

  if (m_collisionType == "PbPb18" && m_Akt4HI_Insitu_CalibTool) {
    delete m_Akt4HI_Insitu_CalibTool;
    m_Akt4HI_Insitu_CalibTool = nullptr;
  }


  Info ("finalize ()", "Finished deleting tool handles, all done now.");


  
  return StatusCode::SUCCESS;
}





/////////////////////////////////////////////////////////////////////                                                                                                       
// 2018 out-of-time pile-up removal                                                                                                                                         
/////////////////////////////////////////////////////////////////////                                                                                                       
bool MyJetxAODAnalysis :: is_Outpileup(const xAOD::HIEventShapeContainer& evShCont, const int nTrack) {

  if (nTrack > 3000) // The selection is only for [0, 3000]                                                                                                                 
    return 0;

  float Fcal_Et = 0.0;
  float Tot_Et = 0.0;
  float oop_Et = 0.0;
  Fcal_Et = evShCont.at(5)->et()*1e-6;
  Tot_Et = evShCont.at(0)->et()*1e-6;
  oop_Et = Tot_Et - Fcal_Et;// Barrel + Endcap calo                                                                                                                         

  int nBin{m_oop_hMean->GetXaxis()->FindFixBin(nTrack)};
  double mean{m_oop_hMean->GetBinContent(nBin)};
  double sigma{m_oop_hSigma->GetBinContent(nBin)};

  ANA_MSG_DEBUG (" oop_Et = " << oop_Et << "TeV"  );

  if (m_nside == 1) // one side cut                                                                                                                                         
    if (oop_Et - mean > -4 * sigma) // 4 sigma cut                                                                                                                          
      return 0;

  if (m_nside == 2) // two side cut                                                                                                                                         
    if (abs(oop_Et - mean) < 4 * sigma) // 4 sigma cut                                                                                                                      
      return 0;

  return 1;
}

