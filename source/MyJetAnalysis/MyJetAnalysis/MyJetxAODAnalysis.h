#ifndef MyJetAnalysis_MyJetxAODAnalysis_H
#define MyJetAnalysis_MyJetxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <iostream>

// Trigger                                                                                       
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>

// ROOT                                                                                                                           
#include <TH1.h>
#include <TTree.h>

#include "AsgTools/AnaToolHandle.h"

// std includes                                                                                  
#include <string>
#include <vector>
#include <map> 


// Jet calibration tool
#include "JetCalibTools/JetCalibrationTool.h"

// Jet cleaning tool
#include "JetSelectorTools/JetCleaningTool.h"


// E/gamma tools                                                                                                                  
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"

// Track selection tool
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// HI event shape access
#include "xAODHIEvent/HIEventShapeContainer.h"

//Truth tool
#include "MCTruthClassifier/MCTruthClassifier.h"

// Trigger
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"



class MyJetxAODAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyJetxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

 private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
  bool mCollissions = true; //!  
  //mCollisions is set to false if running over data
  bool track_bool = true;
  //Leave it 1 if you want tracks
  std::string m_outputName;

  TTree* mytree; //!
  
  TH1D* m_oop_hMean = nullptr;
  TH1D* m_oop_hSigma = nullptr;

  uint8_t getSum(const xAOD::TrackParticle& trk, xAOD::SummaryType sumType); 
  MCTruthClassifier* m_truthClassifier;//!



  //----pp or Pb+Pb MC
  std::string m_typeMC = "ppMC"; //!
  //std::string m_typeMC = "PbPbMC"; //!
  std::string m_collisionType = "PbPb18"; //!

                                                             
  //HLT_J* Triggers //!
  //trigger tools                                                                                                                       
  asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool;
  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool;

  // Triggering tools
  Trig::TrigDecisionTool* m_trigDecisionTool_1; //!
  TrigConf::xAODConfigTool* m_trigConfigTool_1; //!


  //track tools & Necessary Files
  const std::string m_oop_fname = "all_fit.root";

  // Track selection tool
  InDet::InDetTrackSelectionTool* m_trackSelectionToolTightPrimary; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHILoose; //!
  InDet::InDetTrackSelectionTool* m_trackSelectionToolHITight; //!



  const int m_nside = 1;
  //----Calibration Tools
  //----Data
  std::string m_configFile;

  // Jet cleaning tool
  //DoUgly is FALSE
  JetCleaningTool* m_jetCleaningTool; //!
  //DoUgly is TRUE
  JetCleaningTool* m_jetCleaningTool_DoUgly; //!   


  //DoUgly is FALSE                                                                                                                                                                                                                    
  JetCleaningTool* m_jetCleaningTool_MediumBad; //!                                                                                                                                                                                       
  //DoUgly is TRUE                                                                                                                                                                                                                     
  JetCleaningTool* m_jetCleaningTool_MediumBad_DoUgly; //!   

  //DoUgly is FALSE                                                                                                                                                                                                                       
  JetCleaningTool* m_jetCleaningTool_TightBad; //!                                                                                                                                                                                       
  //DoUgly is TRUE                                                                                                                                                                                                                        
  JetCleaningTool* m_jetCleaningTool_TightBad_DoUgly; //!                                                                                                                                                                              




  // EtaJES calibration tool - EM scale
  JetCalibrationTool* m_Akt10HI_EM_EtaJES_CalibTool; //!
  // 2015 insitu + cross calibration tool
  JetCalibrationTool* m_Akt10HI_Insitu_CalibTool; //!
  
  // EtaJES calibration tool - EM scale                                                                                                                                                                   
  JetCalibrationTool* m_Akt4HI_EM_EtaJES_CalibTool; //!                                                                                                                                                   
  // 2015 insitu + cross calibration tool                                                                                                                                                                
  JetCalibrationTool* m_Akt4HI_Insitu_CalibTool; //!   
 

  
  //bool HLT_j200_a10_ion_L1J50 = false; //!
  // Tree branches                                                                          
  int m_b_runNumber; //!                                                                  
  int m_b_eventNumber; //!                                                                  
  
  bool HLT_j180_a10_ion_L1J50 =false; //!
  bool HLT_j200_a10_ion_L1J50 = false; //!
  bool HLT_j150_a10_ion_L1J50 = false; //!

  bool HLT_j110_a10_lcw_subjes_L1J30 = false; //!
  bool HLT_j110 = false; //!

  //---Out ot Time Pile Up
  bool m_b_isOOTPU; //!


  // fcal energy                                                                         
  float m_b_fcalA_et; //!                                                  
  float m_b_fcalC_et; //!                                                               
                                                                                     
  //----Track Cuts                                                                            
  const float m_trkPtCut = 3;
  const float m_truthTrkPtCut = 3;

  float m_b_pthat;

  //--Un-Calibrated Jets 
  int m_b_akt10hi_jet_n; //!                                                        

  std::vector<bool> m_b_LooseBad_10; //!
  std::vector<bool> m_b_LooseBad_DoUgly_10; //!
  std::vector<bool> m_b_MediumBad_10; //!
  std::vector<bool> m_b_MediumBad_DoUgly_10; //!
  std::vector<bool> m_b_TightBad_10; //!
  std::vector<bool> m_b_TightBad_DoUgly_10; //!

  std::vector<float> m_b_akt10hi_constit_jet_pt; //!                                     
  std::vector<float> m_b_akt10hi_constit_jet_eta; //!                                    
  std::vector<float> m_b_akt10hi_constit_jet_phi; //!                                     
  std::vector<float> m_b_akt10hi_constit_jet_e; //!  

  std::vector<float> m_b_akt10hi_em_jet_pt; //!                                      
  std::vector<float> m_b_akt10hi_em_jet_eta; //!
  std::vector<float> m_b_akt10hi_em_jet_phi; //!                                        
  std::vector<float> m_b_akt10hi_em_jet_e; //! 
  //---Calibrated Jets
  std::vector<float> m_b_akt10hi_jet_Insitu_calib_pt; //!                       
  std::vector<float> m_b_akt10hi_jet_Insitu_calib_eta; //!                           
  std::vector<float> m_b_akt10hi_jet_Insitu_calib_phi; //!  
  std::vector<float> m_b_akt10hi_jet_Insitu_calib_e; //!                       

  //--Un-Calibrated Jets                                                                                                                                            
  int m_b_akt4hi_jet_n; //!                                                                                                                                        
  
  std::vector<bool> m_b_LooseBad_4; //!                                                                                                                                                  
  std::vector<bool> m_b_LooseBad_DoUgly_4; //!                                                                                                                                          
  std::vector<bool> m_b_MediumBad_4; //!                                                                                                                                                 
  std::vector<bool> m_b_MediumBad_DoUgly_4; //!                                                                                                                                        
  std::vector<bool> m_b_TightBad_4; //!                                                                                                                                               
  std::vector<bool> m_b_TightBad_DoUgly_4; //! 

  std::vector<float> m_b_akt4hi_constit_jet_pt; //!                                                                                                                
  std::vector<float> m_b_akt4hi_constit_jet_eta; //!                                                                                                               
  std::vector<float> m_b_akt4hi_constit_jet_phi; //!                                                                                                               
  std::vector<float> m_b_akt4hi_constit_jet_e; //!                                                                                                                 

  std::vector<float> m_b_akt4hi_em_jet_pt; //!                                                                                                                     
  std::vector<float> m_b_akt4hi_em_jet_eta; //!                                                                                                                   
  std::vector<float> m_b_akt4hi_em_jet_phi; //!                                                                                                                   
  std::vector<float> m_b_akt4hi_em_jet_e; //!                                                                                                                    
  //---Calibrated Jets                                                                                                                                             
  std::vector<float> m_b_akt4hi_jet_Insitu_calib_pt; //!                                                                                                         
  std::vector<float> m_b_akt4hi_jet_Insitu_calib_eta; //!                                                                                                        
  std::vector<float> m_b_akt4hi_jet_Insitu_calib_phi; //!                                                                                                       
  std::vector<float> m_b_akt4hi_jet_Insitu_calib_e; //!   





  //---Un-Calibrated Jets
  int m_b_akt10_truth_jet_n; //!                                                         
  std::vector<float> m_b_akt10_truth_jet_pt; //!                                   
  std::vector<float> m_b_akt10_truth_jet_eta; //!                            
  std::vector<float> m_b_akt10_truth_jet_phi; //!                             
  std::vector<float> m_b_akt10_truth_jet_e; //!

  int m_b_akt4_truth_jet_n; //!                                                                                                                                 

  std::vector<float> m_b_akt4_truth_jet_pt; //!                                                                                                                   
  std::vector<float> m_b_akt4_truth_jet_eta; //!                                                                                                                
  std::vector<float> m_b_akt4_truth_jet_phi; //!                                                                                                               
  std::vector<float> m_b_akt4_truth_jet_e; //!                                                                                                                 




  //---Reco Jets
  std::vector<float> m_b_akt10_reco_sub_et; //!
  std::vector<float> m_b_akt10_reco_sub_e; //!

  std::vector<float> m_b_akt10_reco_SubET; //!

  std::vector<float> m_b_akt10_reco_jet_etaJES_calib_pt; //!
  std::vector<float> m_b_akt10_reco_jet_etaJES_calib_eta; //!
  std::vector<float> m_b_akt10_reco_jet_etaJES_calib_phi; //!
  std::vector<float> m_b_akt10_reco_jet_etaJES_calib_e; //!

  std::vector<float> m_b_akt10_reco_constit_jet_pt; //!
  std::vector<float> m_b_akt10_reco_constit_jet_eta; //!
  std::vector<float> m_b_akt10_reco_constit_jet_phi; //!
  std::vector<float> m_b_akt10_reco_constit_jet_e; //!

  std::vector<float> m_b_akt10_reco_em_jet_pt; //!
  std::vector<float> m_b_akt10_reco_em_jet_eta; //!
  std::vector<float> m_b_akt10_reco_em_jet_phi; //!
  std::vector<float> m_b_akt10_reco_em_jet_e; //!

  std::vector<float> m_b_akt10_reco_jet_Insitu_calib_pt; //!
  std::vector<float> m_b_akt10_reco_jet_Insitu_calib_eta; //!
  std::vector<float> m_b_akt10_reco_jet_Insitu_calib_phi; //!
  std::vector<float> m_b_akt10_reco_jet_Insitu_calib_e; //!

  int m_b_akt10_reco_jet_n = 0; //!


  std::vector<float> m_b_akt4_reco_sub_et; //!                                                                                                                 
  std::vector<float> m_b_akt4_reco_sub_e; //!                                                                                                                  
  
  std::vector<float> m_b_akt4_reco_SubET; //!
  
  std::vector<float> m_b_akt4_reco_jet_etaJES_calib_pt; //!                                                                                                    
  std::vector<float> m_b_akt4_reco_jet_etaJES_calib_eta; //!                                                                                                   
  std::vector<float> m_b_akt4_reco_jet_etaJES_calib_phi; //!                                                                                                   
  std::vector<float> m_b_akt4_reco_jet_etaJES_calib_e; //!                                                                                                         
  std::vector<float> m_b_akt4_reco_constit_jet_pt; //!                                                                                                           
  std::vector<float> m_b_akt4_reco_constit_jet_eta; //!                                                                                                      
  std::vector<float> m_b_akt4_reco_constit_jet_phi; //!                                                                                                        
  std::vector<float> m_b_akt4_reco_constit_jet_e; //!                                                                                                              

  std::vector<float> m_b_akt4_reco_em_jet_pt; //!                                                                                                              
  std::vector<float> m_b_akt4_reco_em_jet_eta; //!                                                                                                               
  std::vector<float> m_b_akt4_reco_em_jet_phi; //!                                                                                                              
  std::vector<float> m_b_akt4_reco_em_jet_e; //!                                                                                                                

  std::vector<float> m_b_akt4_reco_jet_Insitu_calib_pt; //!                                                                                             
  std::vector<float> m_b_akt4_reco_jet_Insitu_calib_eta; //!                                                                                                   
  std::vector<float> m_b_akt4_reco_jet_Insitu_calib_phi; //!                                                                                                   
  std::vector<float> m_b_akt4_reco_jet_Insitu_calib_e; //!                                                                                                      

  int m_b_akt4_reco_jet_n = 0; //!




  //------Truth Tracks 
  int m_b_truth_n; //!
  std::vector<float> m_b_truth_charge; //!
  std::vector<float> m_b_truth_pt; //!
  std::vector<float> m_b_truth_e; //!
  std::vector<float> m_b_truth_eta; //!
  std::vector<float> m_b_truth_phi; //!
  std::vector<int> m_b_truth_status; //!
  std::vector<int> m_b_truth_pdg; //!
  std::vector<int> m_b_truth_type;
  std::vector<int> m_b_truth_origin;


  //----Tracks
  int m_b_ntrk; //!
  std::vector<float> m_b_trk_pt; //!
  std::vector<float> m_b_trk_eta; //!
  std::vector<float> m_b_trk_phi; //!
  std::vector<float> m_b_trk_charge; //!
  std::vector<bool> m_b_trk_tightPrimary; //!
  std::vector<bool> m_b_trk_HILoose; //!
  std::vector<bool> m_b_trk_HITight; //!
  std::vector<float> m_b_trk_d0; //!
  std::vector<float> m_b_trk_z0; //!
  std::vector<float> m_b_trk_vz; //!
  std::vector<float> m_b_trk_theta; //!
  std::vector<int> m_b_trk_nPixelHits; //!
  std::vector<int> m_b_trk_nSCTHits; //!
  std::vector<int> m_b_trk_nBlayerHits; //!   

  static const int m_b_nTreeParton = 2;
  float m_b_treePartonPt[m_b_nTreeParton];
  float m_b_treePartonEta[m_b_nTreeParton];
  float m_b_treePartonPhi[m_b_nTreeParton];
  int m_b_treePartonId[m_b_nTreeParton];



  // vertex info
  const static int m_max_nvert = 30;
  int m_b_nvert; //!
  float m_b_vert_x[m_max_nvert]; //!
  float m_b_vert_y[m_max_nvert]; //!
  float m_b_vert_z[m_max_nvert]; //!
  int m_b_vert_ntrk[m_max_nvert]; //!
  int m_b_vert_type[m_max_nvert]; //!
  float m_b_vert_sumpt[m_max_nvert]; //!


  //----SubtracteET 
  float ET_before_sub; //!
  float ET_after_sub; //!
  float subtracted_ET; //!

  // these are the functions inherited from Algorithm
  bool is_Outpileup (const xAOD::HIEventShapeContainer& evShCont, const int nTrack);



};

#endif
