#include <iostream>
#include <cstdlib>

void ATestSubmit (const std::string& submitDir)
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //const char* inputFilePath = gSystem->ExpandPathName ("$ALRB_TutorialData/r9315/");
  //SH::ScanDir().filePattern("AOD.11182705._000001.pool.root.1").scan(sh,inputFilePath);
  //SH::scanRucio (sh, "data18_hi.periodAllYear.physics_HardProbes.PhysCont.AOD.t0pro22_v01");
  
  //--------Pb+Pb Data/MC----------//
  //SH::scanRucio (sh, "data18_hi.periodAllYear.physics_HardProbes.PhysCont.AOD.t0pro22_v01/"); //Pb+Pb 2018 HP Stream
  //SH::scanRucio (sh, "mc16_5TeV.420015.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5R04.merge.AOD.e4108_d1516_r11439_r11217"); //00
  //SH::scanRucio (sh, "mc16_5TeV.420014.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4R04.merge.AOD.e4108_d1516_r11439_r11217"); //01
  //SH::scanRucio (sh, "mc16_5TeV.420013.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3R04.merge.AOD.e4108_d1516_r11439_r11217");
  //SH::scanRucio (sh, "mc16_5TeV.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04.merge.AOD.e4108_d1516_r11439_r11217"); //JZ2 (60-160 GeV) 

  //SH::scanRucio (sh,"data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_ZMUMU.repro21_v01/");
  //--DAOD_HION9
  //SH::scanRucio (sh,"data18_hi.00365681.physics_HardProbes.deriv.DAOD_HION9.f1021_m2037_p3901/");  //01
  //SH::scanRucio (sh,"data18_hi.00365752.physics_HardProbes.deriv.DAOD_HION9.f1021_m2037_p3901/");  //02

  
  //-------pp Data/MC--------//
  //SH::scanRucio (sh, "data17_5TeV.periodM.physics_Main.PhysCont.AOD.pro23_v02");
  //SH::scanRucio (sh, "data17_5TeV.00340718.physics_Main.merge.AOD.r11382_p3827_tid17838594_00/"); //pp Data --Run #340718
  
  //SH::scanRucio (sh, "mc16_5TeV.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04.recon.AOD.e4108_s3238_r11199"); //JZ2 (60-160 GeV) --DONE
  //SH::scanRucio (sh, "mc16_5TeV.420012.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2R04.recon.AOD.e6608_s3238_r11199"); //JZ2 (60-160 GeV) --DONE

  SH::scanRucio (sh, "mc16_5TeV.420013.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3R04.recon.AOD.e4108_s3238_r11199"); //JZ3 (160-400 GeV) --DONE
  //SH::scanRucio (sh, "mc16_5TeV.420013.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3R04.recon.AOD.e6608_s3238_r11199"); //JZ3 (160-400 GeV) --DONE 

  //SH::scanRucio (sh, "mc16_5TeV.420014.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4R04.recon.AOD.e4108_s3238_r11199"); //JZ4 (400-800 GeV) --DONE 
  //SH::scanRucio (sh, "mc16_5TeV.420014.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4R04.recon.AOD.e6608_s3238_r11199"); //JZ4 (400-800 GeV) --DONE   

  //SH::scanRucio (sh, "mc16_5TeV.420015.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5R04.recon.AOD.e4108_s3238_r11199"); //JZ5 (800-1600 GeV) --DONE  
  //SH::scanRucio (sh, "mc16_5TeV.420015.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5R04.recon.AOD.e6608_s3238_r11199"); //JZ5 (800-1600 GeV)   



  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");
  sh.setMetaString ("nc_grid_filter", "*AOD*");
  //  sh.load ("ANALYSIS");
  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  //job.options()->setDouble (EL::Job::optMaxEvents, 500); // for testing purposes, limit to run over the first 500 events only!
  //job.options()->setString( EL::Job::optSubmitDirMode, "unique-link");
  job.outputAdd (EL::OutputStream ("myOutput"));
  // add our algorithm to the job
  //EL::OutputStream output ("myOutput");
  //job.outputAdd (output);
  
  
  EL::AnaAlgorithmConfig alg;
  alg.setType ("MyJetxAODAnalysis");
  //alg.m_outputName = "myOutput"; // give the name of the output to our algorithm 
  // set the name of the algorithm (this is the name use with
  // messages)
  alg.setName ("AnalysisAlg");
  
  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);

  // make the driver we want to use:
  // this one works by running the algorithm directly:
  //EL::DirectDriver driver;
  EL::PrunDriver driver;
  driver.options()->setString("nc_outputSampleName", "user.berenice.04202021.LargeRJet_ppMC_JZ3_part1_Analysis_2016_ppHI.00000000000");
  // we can use other drivers to run things on the Grid, with PROOF, etc.
  //driver.options ()->setString (EL::Job::optGridNFiles, "1000");
  //driver.options ()->setString (EL::Job::optGridNJobs, "500"); //10-File0
  
  driver.options ()->setString (EL::Job::optGridNFilesPerJob, "5");  //400--01,50,--02,
  // process the job using the driver
  //sleep(1800);
  driver.submit (job, submitDir);
}
