# Generated by CMake

if("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" LESS 2.5)
   message(FATAL_ERROR "CMake >= 2.6.0 required")
endif()
cmake_policy(PUSH)
cmake_policy(VERSION 2.6)
#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Protect against multiple inclusion, which would fail when already imported targets are added once more.
set(_targetsDefined)
set(_targetsNotDefined)
set(_expectedTargets)
foreach(_expectedTarget WorkDir::ElectronPhotonFourMomentumCorrectionPkg WorkDir::ElectronPhotonFourMomentumCorrectionPkgPrivate WorkDir::ElectronPhotonFourMomentumCorrectionLib WorkDir::ElectronPhotonFourMomentumCorrection_test_memory WorkDir::ElectronPhotonFourMomemtumCorrection_testMomentumSystematics WorkDir::ElectronPhotonFourMomemtumCorrection_testResolutionParam WorkDir::ElectronPhotonFourMomentumCorrection_testUniformityCorrections WorkDir::ElectronPhotonFourMomentumCorrection_testEgammaCalibTool WorkDir::ZdcAnalysisPkg WorkDir::ZdcAnalysisPkgPrivate WorkDir::ZdcAnalysisLib WorkDir::MyJetAnalysisPkg WorkDir::MyJetAnalysisPkgPrivate WorkDir::MyJetAnalysisLib WorkDir::HIEventUtilsPkg WorkDir::HIEventUtilsPkgPrivate WorkDir::HIEventUtilsLib WorkDir::JetCalibToolsPkg WorkDir::JetCalibToolsPkgPrivate WorkDir::JetCalibToolsLib WorkDir::JetCalibTools_Example WorkDir::JetCalibTools_PlotJESFactors WorkDir::JetCalibTools_PlotJMSFactors WorkDir::JetCalibTools_SmearingPlots)
  list(APPEND _expectedTargets ${_expectedTarget})
  if(NOT TARGET ${_expectedTarget})
    list(APPEND _targetsNotDefined ${_expectedTarget})
  endif()
  if(TARGET ${_expectedTarget})
    list(APPEND _targetsDefined ${_expectedTarget})
  endif()
endforeach()
if("${_targetsDefined}" STREQUAL "${_expectedTargets}")
  unset(_targetsDefined)
  unset(_targetsNotDefined)
  unset(_expectedTargets)
  set(CMAKE_IMPORT_FILE_VERSION)
  cmake_policy(POP)
  return()
endif()
if(NOT "${_targetsDefined}" STREQUAL "")
  message(FATAL_ERROR "Some (but not all) targets in this export set were already defined.\nTargets Defined: ${_targetsDefined}\nTargets not yet defined: ${_targetsNotDefined}\n")
endif()
unset(_targetsDefined)
unset(_targetsNotDefined)
unset(_expectedTargets)


# Compute the installation prefix relative to this file.
get_filename_component(_IMPORT_PREFIX "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH)
if(_IMPORT_PREFIX STREQUAL "/")
  set(_IMPORT_PREFIX "")
endif()

# Create imported target WorkDir::ElectronPhotonFourMomentumCorrectionPkg
add_library(WorkDir::ElectronPhotonFourMomentumCorrectionPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::ElectronPhotonFourMomentumCorrectionPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/ElectronPhotonFourMomentumCorrection"
  INTERFACE_LINK_LIBRARIES "AsgToolsPkg;xAODCaloEventPkg;xAODEgammaPkg;xAODEventInfoPkg;PATCorePkg;PATInterfacesPkg;EgammaAnalysisInterfacesPkg"
)

# Create imported target WorkDir::ElectronPhotonFourMomentumCorrectionPkgPrivate
add_library(WorkDir::ElectronPhotonFourMomentumCorrectionPkgPrivate INTERFACE IMPORTED)

set_target_properties(WorkDir::ElectronPhotonFourMomentumCorrectionPkgPrivate PROPERTIES
  INTERFACE_LINK_LIBRARIES "xAODBasePkg;xAODCorePkg;xAODTrackingPkg;xAODMetaDataPkg;egammaLayerRecalibToolPkg;egammaMVACalibPkg;xAODTruthPkg;PathResolverPkg;xAODRootAccessPkg"
)

# Create imported target WorkDir::ElectronPhotonFourMomentumCorrectionLib
add_library(WorkDir::ElectronPhotonFourMomentumCorrectionLib SHARED IMPORTED)

set_target_properties(WorkDir::ElectronPhotonFourMomentumCorrectionLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::ElectronPhotonFourMomentumCorrectionPkg,INTERFACE_INCLUDE_DIRECTORIES>;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/include"
  INTERFACE_LINK_LIBRARIES "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libCore.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libTree.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libMathCore.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libHist.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libRIO.so;/usr/lib64/libpthread.so;AsgTools;xAODCaloEvent;xAODEgamma;xAODEventInfo;PATInterfaces;EgammaAnalysisInterfacesLib;PATCoreLib;egammaMVACalibLib;xAODTruth"
  INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/include"
)

# Create imported target WorkDir::ElectronPhotonFourMomentumCorrection_test_memory
add_executable(WorkDir::ElectronPhotonFourMomentumCorrection_test_memory IMPORTED)

# Create imported target WorkDir::ElectronPhotonFourMomemtumCorrection_testMomentumSystematics
add_executable(WorkDir::ElectronPhotonFourMomemtumCorrection_testMomentumSystematics IMPORTED)

# Create imported target WorkDir::ElectronPhotonFourMomemtumCorrection_testResolutionParam
add_executable(WorkDir::ElectronPhotonFourMomemtumCorrection_testResolutionParam IMPORTED)

# Create imported target WorkDir::ElectronPhotonFourMomentumCorrection_testUniformityCorrections
add_executable(WorkDir::ElectronPhotonFourMomentumCorrection_testUniformityCorrections IMPORTED)

# Create imported target WorkDir::ElectronPhotonFourMomentumCorrection_testEgammaCalibTool
add_executable(WorkDir::ElectronPhotonFourMomentumCorrection_testEgammaCalibTool IMPORTED)

# Create imported target WorkDir::ZdcAnalysisPkg
add_library(WorkDir::ZdcAnalysisPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::ZdcAnalysisPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/ForwardDetectors/ZDC/ZdcAnalysis"
  INTERFACE_LINK_LIBRARIES "AsgToolsPkg;xAODForwardPkg;xAODTrigL1CaloPkg;AnaAlgorithmPkg"
)

# Create imported target WorkDir::ZdcAnalysisPkgPrivate
add_library(WorkDir::ZdcAnalysisPkgPrivate INTERFACE IMPORTED)

set_target_properties(WorkDir::ZdcAnalysisPkgPrivate PROPERTIES
  INTERFACE_LINK_LIBRARIES "PathResolverPkg;xAODEventInfoPkg"
)

# Create imported target WorkDir::ZdcAnalysisLib
add_library(WorkDir::ZdcAnalysisLib SHARED IMPORTED)

set_target_properties(WorkDir::ZdcAnalysisLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::ZdcAnalysisPkg,INTERFACE_INCLUDE_DIRECTORIES>;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/include"
  INTERFACE_LINK_LIBRARIES "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libCore.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libTree.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libMathCore.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libHist.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libRIO.so;/usr/lib64/libpthread.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libMathMore.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libMinuit.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libMinuit2.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libMatrix.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libPhysics.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libHistPainter.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libRint.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libGraf.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libGraf3d.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libGpad.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libHtml.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libPostscript.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libGui.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libGX11TTF.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libGX11.so;AsgTools;xAODForward;xAODTrigL1Calo;AnaAlgorithmLib"
  INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/include"
)

# Create imported target WorkDir::MyJetAnalysisPkg
add_library(WorkDir::MyJetAnalysisPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::MyJetAnalysisPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/MyJetAnalysis"
)

# Create imported target WorkDir::MyJetAnalysisPkgPrivate
add_library(WorkDir::MyJetAnalysisPkgPrivate INTERFACE IMPORTED)

# Create imported target WorkDir::MyJetAnalysisLib
add_library(WorkDir::MyJetAnalysisLib SHARED IMPORTED)

set_target_properties(WorkDir::MyJetAnalysisLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::MyJetAnalysisPkg,INTERFACE_INCLUDE_DIRECTORIES>"
  INTERFACE_LINK_LIBRARIES "PathResolver;AnaAlgorithmLib;EventLoop;EventLoopAlgs;xAODEventInfo;xAODTrigger;AsgAnalysisInterfaces;TrigDecisionToolLib;TrigConfInterfaces;xAODHIEvent;GoodRunsListsLib;TrigConfxAODLib;InDetTrackSelectionToolLib;xAODJet;WorkDir::JetCalibToolsLib;JetSelectorToolsLib;xAODTruth;MCTruthClassifierLib;xAODEgamma;ElectronPhotonSelectorToolsLib;WorkDir::ElectronPhotonFourMomentumCorrectionLib;MuonSelectorToolsLib;MuonMomentumCorrectionsLib"
)

# Create imported target WorkDir::HIEventUtilsPkg
add_library(WorkDir::HIEventUtilsPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::HIEventUtilsPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/PhysicsAnalysis/HeavyIonPhys/HIEventUtils"
  INTERFACE_LINK_LIBRARIES "CaloGeoHelpersPkg;AsgToolsPkg;xAODCaloEventPkg;xAODEventInfoPkg;xAODForwardPkg;xAODHIEventPkg;xAODTrackingPkg;xAODTrigL1CaloPkg;InDetTrackSelectionToolPkg;WorkDir::ZdcAnalysisPkg;PathResolverPkg;PATCorePkg"
)

# Create imported target WorkDir::HIEventUtilsPkgPrivate
add_library(WorkDir::HIEventUtilsPkgPrivate INTERFACE IMPORTED)

set_target_properties(WorkDir::HIEventUtilsPkgPrivate PROPERTIES
  INTERFACE_LINK_LIBRARIES "CxxUtilsPkg"
)

# Create imported target WorkDir::HIEventUtilsLib
add_library(WorkDir::HIEventUtilsLib SHARED IMPORTED)

set_target_properties(WorkDir::HIEventUtilsLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::HIEventUtilsPkg,INTERFACE_INCLUDE_DIRECTORIES>;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/include"
  INTERFACE_LINK_LIBRARIES "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libCore.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libTree.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libMathCore.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libHist.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libRIO.so;/usr/lib64/libpthread.so;CaloGeoHelpers;AsgTools;xAODCaloEvent;xAODEventInfo;xAODForward;xAODHIEvent;xAODTracking;xAODTrigL1Calo;PathResolver;PATCoreLib"
  INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/include"
)

# Create imported target WorkDir::JetCalibToolsPkg
add_library(WorkDir::JetCalibToolsPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::JetCalibToolsPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/Reconstruction/Jet/JetCalibTools"
  INTERFACE_LINK_LIBRARIES "AsgToolsPkg;xAODEventInfoPkg;xAODEventShapePkg;xAODJetPkg;xAODTrackingPkg;PATInterfacesPkg;JetInterfacePkg"
)

# Create imported target WorkDir::JetCalibToolsPkgPrivate
add_library(WorkDir::JetCalibToolsPkgPrivate INTERFACE IMPORTED)

set_target_properties(WorkDir::JetCalibToolsPkgPrivate PROPERTIES
  INTERFACE_LINK_LIBRARIES "xAODMuonPkg;PathResolverPkg;POOLRootAccessPkg;xAODRootAccessPkg"
)

# Create imported target WorkDir::JetCalibToolsLib
add_library(WorkDir::JetCalibToolsLib SHARED IMPORTED)

set_target_properties(WorkDir::JetCalibToolsLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::JetCalibToolsPkg,INTERFACE_INCLUDE_DIRECTORIES>;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/include"
  INTERFACE_LINK_LIBRARIES "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libCore.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libTree.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libMathCore.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libHist.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libRIO.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libGraf.so;/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/lib/libGpad.so;AsgTools;xAODEventInfo;xAODEventShape;xAODJet;xAODTracking;PATInterfaces;JetInterface"
  INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.111/InstallArea/x86_64-centos7-gcc8-opt/include"
)

# Create imported target WorkDir::JetCalibTools_Example
add_executable(WorkDir::JetCalibTools_Example IMPORTED)

# Create imported target WorkDir::JetCalibTools_PlotJESFactors
add_executable(WorkDir::JetCalibTools_PlotJESFactors IMPORTED)

# Create imported target WorkDir::JetCalibTools_PlotJMSFactors
add_executable(WorkDir::JetCalibTools_PlotJMSFactors IMPORTED)

# Create imported target WorkDir::JetCalibTools_SmearingPlots
add_executable(WorkDir::JetCalibTools_SmearingPlots IMPORTED)

if(CMAKE_VERSION VERSION_LESS 3.0.0)
  message(FATAL_ERROR "This file relies on consumers using CMake 3.0.0 or greater.")
endif()

# Load information for each installed configuration.
get_filename_component(_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
file(GLOB CONFIG_FILES "${_DIR}/WorkDirConfig-targets-*.cmake")
foreach(f ${CONFIG_FILES})
  include(${f})
endforeach()

# Cleanup temporary variables.
set(_IMPORT_PREFIX)

# Loop over all imported files and verify that they actually exist
foreach(target ${_IMPORT_CHECK_TARGETS} )
  foreach(file ${_IMPORT_CHECK_FILES_FOR_${target}} )
    if(NOT EXISTS "${file}" )
      message(FATAL_ERROR "The imported target \"${target}\" references the file
   \"${file}\"
but this file does not exist.  Possible reasons include:
* The file was deleted, renamed, or moved to another location.
* An install or uninstall procedure did not complete successfully.
* The installation package was faulty and contained
   \"${CMAKE_CURRENT_LIST_FILE}\"
but not all the files it references.
")
    endif()
  endforeach()
  unset(_IMPORT_CHECK_FILES_FOR_${target})
endforeach()
unset(_IMPORT_CHECK_TARGETS)

# This file does not depend on other imported targets which have
# been exported from the same project but in a separate export set.

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
cmake_policy(POP)
