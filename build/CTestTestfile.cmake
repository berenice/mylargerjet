# CMake generated Testfile for 
# Source directory: /afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/source
# Build directory: /afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("ElectronPhotonFourMomentumCorrection")
subdirs("ForwardDetectors/ZDC/ZdcAnalysis")
subdirs("MyJetAnalysis")
subdirs("PhysicsAnalysis/HeavyIonPhys/HIEventUtils")
subdirs("Reconstruction/Jet/JetCalibTools")
