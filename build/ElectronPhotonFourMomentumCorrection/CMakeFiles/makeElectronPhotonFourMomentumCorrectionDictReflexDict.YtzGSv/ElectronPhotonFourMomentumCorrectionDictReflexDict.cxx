// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME ElectronPhotonFourMomentumCorrectionDictReflexDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/source/ElectronPhotonFourMomentumCorrection/ElectronPhotonFourMomentumCorrection/ElectronPhotonFourMomentumCorrectionDict.h"

// Header files passed via #pragma extra_include

namespace egEnergyCorr {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *egEnergyCorr_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("egEnergyCorr", 0 /*version*/, "egammaEnergyCorrectionTool.h", 55,
                     ::ROOT::Internal::DefineBehavior((void*)0,(void*)0),
                     &egEnergyCorr_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_DICT_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_DICT_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *egEnergyCorr_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace egEnergyCorr {
   namespace Resolution {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *egEnergyCorrcLcLResolution_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("egEnergyCorr::Resolution", 0 /*version*/, "egammaEnergyCorrectionTool.h", 59,
                     ::ROOT::Internal::DefineBehavior((void*)0,(void*)0),
                     &egEnergyCorrcLcLResolution_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_DICT_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_DICT_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *egEnergyCorrcLcLResolution_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}
}

namespace egEnergyCorr {
   namespace Scale {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *egEnergyCorrcLcLScale_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("egEnergyCorr::Scale", 0 /*version*/, "egammaEnergyCorrectionTool.h", 112,
                     ::ROOT::Internal::DefineBehavior((void*)0,(void*)0),
                     &egEnergyCorrcLcLScale_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_DICT_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_DICT_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *egEnergyCorrcLcLScale_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}
}

namespace ROOT {
   static TClass *egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround_Dictionary();
   static void egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround_TClassManip(TClass*);
   static void *new_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(void *p = 0);
   static void *newArray_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(Long_t size, void *p);
   static void delete_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(void *p);
   static void deleteArray_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(void *p);
   static void destruct_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround*)
   {
      ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::egEnergyCorr::ROOT6_OpenNamespaceWorkaround));
      static ::ROOT::TGenericClassInfo 
         instance("egEnergyCorr::ROOT6_OpenNamespaceWorkaround", "egammaEnergyCorrectionTool.h", 56,
                  typeid(::egEnergyCorr::ROOT6_OpenNamespaceWorkaround), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround_Dictionary, isa_proxy, 4,
                  sizeof(::egEnergyCorr::ROOT6_OpenNamespaceWorkaround) );
      instance.SetNew(&new_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround);
      instance.SetNewArray(&newArray_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDelete(&delete_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDeleteArray(&deleteArray_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDestructor(&destruct_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround*)
   {
      return GenerateInitInstanceLocal((::egEnergyCorr::ROOT6_OpenNamespaceWorkaround*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround*)0x0)->GetClass();
      egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround_TClassManip(theClass);
   return theClass;
   }

   static void egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround_Dictionary();
   static void egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround_TClassManip(TClass*);
   static void *new_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(void *p = 0);
   static void *newArray_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(Long_t size, void *p);
   static void delete_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(void *p);
   static void deleteArray_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(void *p);
   static void destruct_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround*)
   {
      ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround));
      static ::ROOT::TGenericClassInfo 
         instance("egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround", "egammaEnergyCorrectionTool.h", 60,
                  typeid(::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround_Dictionary, isa_proxy, 4,
                  sizeof(::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround) );
      instance.SetNew(&new_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround);
      instance.SetNewArray(&newArray_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDelete(&delete_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDeleteArray(&deleteArray_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDestructor(&destruct_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround*)
   {
      return GenerateInitInstanceLocal((::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround*)0x0)->GetClass();
      egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround_TClassManip(theClass);
   return theClass;
   }

   static void egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround_Dictionary();
   static void egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround_TClassManip(TClass*);
   static void *new_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(void *p = 0);
   static void *newArray_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(Long_t size, void *p);
   static void delete_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(void *p);
   static void deleteArray_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(void *p);
   static void destruct_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround*)
   {
      ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround));
      static ::ROOT::TGenericClassInfo 
         instance("egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround", "egammaEnergyCorrectionTool.h", 113,
                  typeid(::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround_Dictionary, isa_proxy, 4,
                  sizeof(::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround) );
      instance.SetNew(&new_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround);
      instance.SetNewArray(&newArray_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDelete(&delete_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDeleteArray(&deleteArray_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround);
      instance.SetDestructor(&destruct_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround*)
   {
      return GenerateInitInstanceLocal((::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround*)0x0)->GetClass();
      egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround_TClassManip(theClass);
   return theClass;
   }

   static void egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AtlasRootcLcLegammaEnergyCorrectionTool_Dictionary();
   static void AtlasRootcLcLegammaEnergyCorrectionTool_TClassManip(TClass*);
   static void *new_AtlasRootcLcLegammaEnergyCorrectionTool(void *p = 0);
   static void *newArray_AtlasRootcLcLegammaEnergyCorrectionTool(Long_t size, void *p);
   static void delete_AtlasRootcLcLegammaEnergyCorrectionTool(void *p);
   static void deleteArray_AtlasRootcLcLegammaEnergyCorrectionTool(void *p);
   static void destruct_AtlasRootcLcLegammaEnergyCorrectionTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AtlasRoot::egammaEnergyCorrectionTool*)
   {
      ::AtlasRoot::egammaEnergyCorrectionTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AtlasRoot::egammaEnergyCorrectionTool));
      static ::ROOT::TGenericClassInfo 
         instance("AtlasRoot::egammaEnergyCorrectionTool", "egammaEnergyCorrectionTool.h", 273,
                  typeid(::AtlasRoot::egammaEnergyCorrectionTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &AtlasRootcLcLegammaEnergyCorrectionTool_Dictionary, isa_proxy, 4,
                  sizeof(::AtlasRoot::egammaEnergyCorrectionTool) );
      instance.SetNew(&new_AtlasRootcLcLegammaEnergyCorrectionTool);
      instance.SetNewArray(&newArray_AtlasRootcLcLegammaEnergyCorrectionTool);
      instance.SetDelete(&delete_AtlasRootcLcLegammaEnergyCorrectionTool);
      instance.SetDeleteArray(&deleteArray_AtlasRootcLcLegammaEnergyCorrectionTool);
      instance.SetDestructor(&destruct_AtlasRootcLcLegammaEnergyCorrectionTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AtlasRoot::egammaEnergyCorrectionTool*)
   {
      return GenerateInitInstanceLocal((::AtlasRoot::egammaEnergyCorrectionTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::AtlasRoot::egammaEnergyCorrectionTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AtlasRootcLcLegammaEnergyCorrectionTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AtlasRoot::egammaEnergyCorrectionTool*)0x0)->GetClass();
      AtlasRootcLcLegammaEnergyCorrectionTool_TClassManip(theClass);
   return theClass;
   }

   static void AtlasRootcLcLegammaEnergyCorrectionTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CPcLcLEgammaCalibrationAndSmearingTool_Dictionary();
   static void CPcLcLEgammaCalibrationAndSmearingTool_TClassManip(TClass*);
   static void delete_CPcLcLEgammaCalibrationAndSmearingTool(void *p);
   static void deleteArray_CPcLcLEgammaCalibrationAndSmearingTool(void *p);
   static void destruct_CPcLcLEgammaCalibrationAndSmearingTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CP::EgammaCalibrationAndSmearingTool*)
   {
      ::CP::EgammaCalibrationAndSmearingTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CP::EgammaCalibrationAndSmearingTool));
      static ::ROOT::TGenericClassInfo 
         instance("CP::EgammaCalibrationAndSmearingTool", "EgammaCalibrationAndSmearingTool.h", 80,
                  typeid(::CP::EgammaCalibrationAndSmearingTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &CPcLcLEgammaCalibrationAndSmearingTool_Dictionary, isa_proxy, 4,
                  sizeof(::CP::EgammaCalibrationAndSmearingTool) );
      instance.SetDelete(&delete_CPcLcLEgammaCalibrationAndSmearingTool);
      instance.SetDeleteArray(&deleteArray_CPcLcLEgammaCalibrationAndSmearingTool);
      instance.SetDestructor(&destruct_CPcLcLEgammaCalibrationAndSmearingTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CP::EgammaCalibrationAndSmearingTool*)
   {
      return GenerateInitInstanceLocal((::CP::EgammaCalibrationAndSmearingTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::CP::EgammaCalibrationAndSmearingTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CPcLcLEgammaCalibrationAndSmearingTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CP::EgammaCalibrationAndSmearingTool*)0x0)->GetClass();
      CPcLcLEgammaCalibrationAndSmearingTool_TClassManip(theClass);
   return theClass;
   }

   static void CPcLcLEgammaCalibrationAndSmearingTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *eg_resolution_Dictionary();
   static void eg_resolution_TClassManip(TClass*);
   static void delete_eg_resolution(void *p);
   static void deleteArray_eg_resolution(void *p);
   static void destruct_eg_resolution(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::eg_resolution*)
   {
      ::eg_resolution *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::eg_resolution));
      static ::ROOT::TGenericClassInfo 
         instance("eg_resolution", "eg_resolution.h", 32,
                  typeid(::eg_resolution), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &eg_resolution_Dictionary, isa_proxy, 4,
                  sizeof(::eg_resolution) );
      instance.SetDelete(&delete_eg_resolution);
      instance.SetDeleteArray(&deleteArray_eg_resolution);
      instance.SetDestructor(&destruct_eg_resolution);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::eg_resolution*)
   {
      return GenerateInitInstanceLocal((::eg_resolution*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::eg_resolution*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *eg_resolution_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::eg_resolution*)0x0)->GetClass();
      eg_resolution_TClassManip(theClass);
   return theClass;
   }

   static void eg_resolution_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *EgammaFactory_Dictionary();
   static void EgammaFactory_TClassManip(TClass*);
   static void *new_EgammaFactory(void *p = 0);
   static void *newArray_EgammaFactory(Long_t size, void *p);
   static void delete_EgammaFactory(void *p);
   static void deleteArray_EgammaFactory(void *p);
   static void destruct_EgammaFactory(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::EgammaFactory*)
   {
      ::EgammaFactory *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::EgammaFactory));
      static ::ROOT::TGenericClassInfo 
         instance("EgammaFactory", "EgammaFactory.h", 39,
                  typeid(::EgammaFactory), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &EgammaFactory_Dictionary, isa_proxy, 4,
                  sizeof(::EgammaFactory) );
      instance.SetNew(&new_EgammaFactory);
      instance.SetNewArray(&newArray_EgammaFactory);
      instance.SetDelete(&delete_EgammaFactory);
      instance.SetDeleteArray(&deleteArray_EgammaFactory);
      instance.SetDestructor(&destruct_EgammaFactory);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::EgammaFactory*)
   {
      return GenerateInitInstanceLocal((::EgammaFactory*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::EgammaFactory*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *EgammaFactory_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::EgammaFactory*)0x0)->GetClass();
      EgammaFactory_TClassManip(theClass);
   return theClass;
   }

   static void EgammaFactory_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *LArTemperatureCorrectionTool_Dictionary();
   static void LArTemperatureCorrectionTool_TClassManip(TClass*);
   static void delete_LArTemperatureCorrectionTool(void *p);
   static void deleteArray_LArTemperatureCorrectionTool(void *p);
   static void destruct_LArTemperatureCorrectionTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::LArTemperatureCorrectionTool*)
   {
      ::LArTemperatureCorrectionTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::LArTemperatureCorrectionTool));
      static ::ROOT::TGenericClassInfo 
         instance("LArTemperatureCorrectionTool", "LArTemperatureCorrectionTool.h", 34,
                  typeid(::LArTemperatureCorrectionTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &LArTemperatureCorrectionTool_Dictionary, isa_proxy, 4,
                  sizeof(::LArTemperatureCorrectionTool) );
      instance.SetDelete(&delete_LArTemperatureCorrectionTool);
      instance.SetDeleteArray(&deleteArray_LArTemperatureCorrectionTool);
      instance.SetDestructor(&destruct_LArTemperatureCorrectionTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::LArTemperatureCorrectionTool*)
   {
      return GenerateInitInstanceLocal((::LArTemperatureCorrectionTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::LArTemperatureCorrectionTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *LArTemperatureCorrectionTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::LArTemperatureCorrectionTool*)0x0)->GetClass();
      LArTemperatureCorrectionTool_TClassManip(theClass);
   return theClass;
   }

   static void LArTemperatureCorrectionTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround : new ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround;
   }
   static void *newArray_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround[nElements] : new ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround[nElements];
   }
   // Wrapper around operator delete
   static void delete_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(void *p) {
      delete ((::egEnergyCorr::ROOT6_OpenNamespaceWorkaround*)p);
   }
   static void deleteArray_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(void *p) {
      delete [] ((::egEnergyCorr::ROOT6_OpenNamespaceWorkaround*)p);
   }
   static void destruct_egEnergyCorrcLcLROOT6_OpenNamespaceWorkaround(void *p) {
      typedef ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::egEnergyCorr::ROOT6_OpenNamespaceWorkaround

namespace ROOT {
   // Wrappers around operator new
   static void *new_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround : new ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround;
   }
   static void *newArray_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround[nElements] : new ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround[nElements];
   }
   // Wrapper around operator delete
   static void delete_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(void *p) {
      delete ((::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround*)p);
   }
   static void deleteArray_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(void *p) {
      delete [] ((::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround*)p);
   }
   static void destruct_egEnergyCorrcLcLResolutioncLcLROOT6_OpenNamespaceWorkaround(void *p) {
      typedef ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround

namespace ROOT {
   // Wrappers around operator new
   static void *new_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround : new ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround;
   }
   static void *newArray_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround[nElements] : new ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround[nElements];
   }
   // Wrapper around operator delete
   static void delete_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(void *p) {
      delete ((::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround*)p);
   }
   static void deleteArray_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(void *p) {
      delete [] ((::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround*)p);
   }
   static void destruct_egEnergyCorrcLcLScalecLcLROOT6_OpenNamespaceWorkaround(void *p) {
      typedef ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround

namespace ROOT {
   // Wrappers around operator new
   static void *new_AtlasRootcLcLegammaEnergyCorrectionTool(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::AtlasRoot::egammaEnergyCorrectionTool : new ::AtlasRoot::egammaEnergyCorrectionTool;
   }
   static void *newArray_AtlasRootcLcLegammaEnergyCorrectionTool(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::AtlasRoot::egammaEnergyCorrectionTool[nElements] : new ::AtlasRoot::egammaEnergyCorrectionTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_AtlasRootcLcLegammaEnergyCorrectionTool(void *p) {
      delete ((::AtlasRoot::egammaEnergyCorrectionTool*)p);
   }
   static void deleteArray_AtlasRootcLcLegammaEnergyCorrectionTool(void *p) {
      delete [] ((::AtlasRoot::egammaEnergyCorrectionTool*)p);
   }
   static void destruct_AtlasRootcLcLegammaEnergyCorrectionTool(void *p) {
      typedef ::AtlasRoot::egammaEnergyCorrectionTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AtlasRoot::egammaEnergyCorrectionTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CPcLcLEgammaCalibrationAndSmearingTool(void *p) {
      delete ((::CP::EgammaCalibrationAndSmearingTool*)p);
   }
   static void deleteArray_CPcLcLEgammaCalibrationAndSmearingTool(void *p) {
      delete [] ((::CP::EgammaCalibrationAndSmearingTool*)p);
   }
   static void destruct_CPcLcLEgammaCalibrationAndSmearingTool(void *p) {
      typedef ::CP::EgammaCalibrationAndSmearingTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CP::EgammaCalibrationAndSmearingTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_eg_resolution(void *p) {
      delete ((::eg_resolution*)p);
   }
   static void deleteArray_eg_resolution(void *p) {
      delete [] ((::eg_resolution*)p);
   }
   static void destruct_eg_resolution(void *p) {
      typedef ::eg_resolution current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::eg_resolution

namespace ROOT {
   // Wrappers around operator new
   static void *new_EgammaFactory(void *p) {
      return  p ? new(p) ::EgammaFactory : new ::EgammaFactory;
   }
   static void *newArray_EgammaFactory(Long_t nElements, void *p) {
      return p ? new(p) ::EgammaFactory[nElements] : new ::EgammaFactory[nElements];
   }
   // Wrapper around operator delete
   static void delete_EgammaFactory(void *p) {
      delete ((::EgammaFactory*)p);
   }
   static void deleteArray_EgammaFactory(void *p) {
      delete [] ((::EgammaFactory*)p);
   }
   static void destruct_EgammaFactory(void *p) {
      typedef ::EgammaFactory current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::EgammaFactory

namespace ROOT {
   // Wrapper around operator delete
   static void delete_LArTemperatureCorrectionTool(void *p) {
      delete ((::LArTemperatureCorrectionTool*)p);
   }
   static void deleteArray_LArTemperatureCorrectionTool(void *p) {
      delete [] ((::LArTemperatureCorrectionTool*)p);
   }
   static void destruct_LArTemperatureCorrectionTool(void *p) {
      typedef ::LArTemperatureCorrectionTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::LArTemperatureCorrectionTool

namespace {
  void TriggerDictionaryInitialization_libElectronPhotonFourMomentumCorrectionDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libElectronPhotonFourMomentumCorrectionDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace egEnergyCorr{struct __attribute__((annotate("$clingAutoload$ElectronPhotonFourMomentumCorrection/egammaEnergyCorrectionTool.h")))  ROOT6_OpenNamespaceWorkaround;}
namespace egEnergyCorr{namespace Resolution{struct __attribute__((annotate("$clingAutoload$ElectronPhotonFourMomentumCorrection/egammaEnergyCorrectionTool.h")))  ROOT6_OpenNamespaceWorkaround;}}
namespace egEnergyCorr{namespace Scale{struct __attribute__((annotate("$clingAutoload$ElectronPhotonFourMomentumCorrection/egammaEnergyCorrectionTool.h")))  ROOT6_OpenNamespaceWorkaround;}}
namespace AtlasRoot{class __attribute__((annotate("$clingAutoload$ElectronPhotonFourMomentumCorrection/egammaEnergyCorrectionTool.h")))  egammaEnergyCorrectionTool;}
namespace CP{class __attribute__((annotate("$clingAutoload$ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h")))  EgammaCalibrationAndSmearingTool;}
class __attribute__((annotate("$clingAutoload$ElectronPhotonFourMomentumCorrection/eg_resolution.h")))  eg_resolution;
class __attribute__((annotate("$clingAutoload$ElectronPhotonFourMomentumCorrection/EgammaFactory.h")))  EgammaFactory;
class __attribute__((annotate("$clingAutoload$ElectronPhotonFourMomentumCorrection/LArTemperatureCorrectionTool.h")))  LArTemperatureCorrectionTool;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libElectronPhotonFourMomentumCorrectionDict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "ElectronPhotonFourMomentumCorrection-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ ElectronPhotonFourMomentumCorrection-00-00-00
#endif
#ifndef EIGEN_DONT_VECTORIZE
  #define EIGEN_DONT_VECTORIZE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// Dear emacs, this is -*-c++-*-
//
// Dictionary generation for this package
//
// - March 2013 : first release
//
#ifndef ELECTRONPHOTONFOURMOMENTUMCORRECTION_ELECTRONPHOTONFOURMOMENTUMCORRECTIONDICT_H
#define ELECTRONPHOTONFOURMOMENTUMCORRECTION_ELECTRONPHOTONFOURMOMENTUMCORRECTIONDICT_H


#include "ElectronPhotonFourMomentumCorrection/egammaEnergyCorrectionTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronPhotonFourMomentumCorrection/eg_resolution.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaFactory.h"
#include "ElectronPhotonFourMomentumCorrection/LArTemperatureCorrectionTool.h"
#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"AtlasRoot::egammaEnergyCorrectionTool", payloadCode, "@",
"CP::EgammaCalibrationAndSmearingTool", payloadCode, "@",
"EgammaFactory", payloadCode, "@",
"LArTemperatureCorrectionTool", payloadCode, "@",
"egEnergyCorr::ESModel", payloadCode, "@",
"egEnergyCorr::Geometry", payloadCode, "@",
"egEnergyCorr::MaterialCategory", payloadCode, "@",
"egEnergyCorr::ROOT6_OpenNamespaceWorkaround", payloadCode, "@",
"egEnergyCorr::Resolution::ROOT6_OpenNamespaceWorkaround", payloadCode, "@",
"egEnergyCorr::Resolution::Variation", payloadCode, "@",
"egEnergyCorr::Resolution::resolutionType", payloadCode, "@",
"egEnergyCorr::Scale::ROOT6_OpenNamespaceWorkaround", payloadCode, "@",
"egEnergyCorr::Scale::Variation", payloadCode, "@",
"eg_resolution", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libElectronPhotonFourMomentumCorrectionDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libElectronPhotonFourMomentumCorrectionDict_Impl, {{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1},{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libElectronPhotonFourMomentumCorrectionDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libElectronPhotonFourMomentumCorrectionDict() {
  TriggerDictionaryInitialization_libElectronPhotonFourMomentumCorrectionDict_Impl();
}
