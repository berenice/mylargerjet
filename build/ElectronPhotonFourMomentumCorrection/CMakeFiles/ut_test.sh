#!/usr/bin/bash
#
# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# This script is used by CTest to run the test ut_test with the correct
# environment setup, and post processing.
#

# Transmit errors:
set -e

# Set up the runtime environment:
source /afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/build/x86_64-centos7-gcc8-opt/setup.sh

# Turn off xAOD monitoring for the test:
export XAOD_ACCESSTRACER_FRACTION=0

# Set the package name, which may be used by post.sh later on:
export ATLAS_CTEST_PACKAGE=ElectronPhotonFourMomentumCorrection

# Run a possible pre-exec script:
# No pre-exec necessary

# Run the test:
/afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/build/ElectronPhotonFourMomentumCorrection/test-bin/ut_test.exe 2>&1 | tee ut_test.log; \
    test ${PIPESTATUS[0]} -eq 0

# Set the test's return code in the variable expected by post.sh:
export testStatus=${PIPESTATUS[0]}

# Put the reference file in place if it exists:
if [ -f /afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/source/ElectronPhotonFourMomentumCorrection/share/ut_test.ref ] &&
    [ "/afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/source/ElectronPhotonFourMomentumCorrection" != "/afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/build/ElectronPhotonFourMomentumCorrection" ]; then
    /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.14.3/Linux-x86_64/bin/cmake -E make_directory ../share
    /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.14.3/Linux-x86_64/bin/cmake -E create_symlink \
     /afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/source/ElectronPhotonFourMomentumCorrection/share/ut_test.ref ../share/ut_test.ref
fi

# Run a post-exec script:
if type post.sh >/dev/null 2>&1; then
    post.sh ut_test ""
fi
