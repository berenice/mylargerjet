// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME ZdcAnalysisDictReflexDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/afs/cern.ch/user/b/berenice/gitDir2/mylargerjet/source/ForwardDetectors/ZDC/ZdcAnalysis/ZdcAnalysis/ZdcAnalysisDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *ZDCcLcLZdcAnalysisAlg_Dictionary();
   static void ZDCcLcLZdcAnalysisAlg_TClassManip(TClass*);
   static void delete_ZDCcLcLZdcAnalysisAlg(void *p);
   static void deleteArray_ZDCcLcLZdcAnalysisAlg(void *p);
   static void destruct_ZDCcLcLZdcAnalysisAlg(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZDC::ZdcAnalysisAlg*)
   {
      ::ZDC::ZdcAnalysisAlg *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ZDC::ZdcAnalysisAlg));
      static ::ROOT::TGenericClassInfo 
         instance("ZDC::ZdcAnalysisAlg", "ZdcAnalysisAlg.h", 15,
                  typeid(::ZDC::ZdcAnalysisAlg), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ZDCcLcLZdcAnalysisAlg_Dictionary, isa_proxy, 4,
                  sizeof(::ZDC::ZdcAnalysisAlg) );
      instance.SetDelete(&delete_ZDCcLcLZdcAnalysisAlg);
      instance.SetDeleteArray(&deleteArray_ZDCcLcLZdcAnalysisAlg);
      instance.SetDestructor(&destruct_ZDCcLcLZdcAnalysisAlg);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZDC::ZdcAnalysisAlg*)
   {
      return GenerateInitInstanceLocal((::ZDC::ZdcAnalysisAlg*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZDC::ZdcAnalysisAlg*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ZDCcLcLZdcAnalysisAlg_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ZDC::ZdcAnalysisAlg*)0x0)->GetClass();
      ZDCcLcLZdcAnalysisAlg_TClassManip(theClass);
   return theClass;
   }

   static void ZDCcLcLZdcAnalysisAlg_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ZDCcLcLZdcAnalysisTool_Dictionary();
   static void ZDCcLcLZdcAnalysisTool_TClassManip(TClass*);
   static void delete_ZDCcLcLZdcAnalysisTool(void *p);
   static void deleteArray_ZDCcLcLZdcAnalysisTool(void *p);
   static void destruct_ZDCcLcLZdcAnalysisTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ZDC::ZdcAnalysisTool*)
   {
      ::ZDC::ZdcAnalysisTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ZDC::ZdcAnalysisTool));
      static ::ROOT::TGenericClassInfo 
         instance("ZDC::ZdcAnalysisTool", "ZdcAnalysisTool.h", 23,
                  typeid(::ZDC::ZdcAnalysisTool), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ZDCcLcLZdcAnalysisTool_Dictionary, isa_proxy, 4,
                  sizeof(::ZDC::ZdcAnalysisTool) );
      instance.SetDelete(&delete_ZDCcLcLZdcAnalysisTool);
      instance.SetDeleteArray(&deleteArray_ZDCcLcLZdcAnalysisTool);
      instance.SetDestructor(&destruct_ZDCcLcLZdcAnalysisTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ZDC::ZdcAnalysisTool*)
   {
      return GenerateInitInstanceLocal((::ZDC::ZdcAnalysisTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ZDC::ZdcAnalysisTool*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ZDCcLcLZdcAnalysisTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ZDC::ZdcAnalysisTool*)0x0)->GetClass();
      ZDCcLcLZdcAnalysisTool_TClassManip(theClass);
   return theClass;
   }

   static void ZDCcLcLZdcAnalysisTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ZDCcLcLZdcAnalysisAlg(void *p) {
      delete ((::ZDC::ZdcAnalysisAlg*)p);
   }
   static void deleteArray_ZDCcLcLZdcAnalysisAlg(void *p) {
      delete [] ((::ZDC::ZdcAnalysisAlg*)p);
   }
   static void destruct_ZDCcLcLZdcAnalysisAlg(void *p) {
      typedef ::ZDC::ZdcAnalysisAlg current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZDC::ZdcAnalysisAlg

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ZDCcLcLZdcAnalysisTool(void *p) {
      delete ((::ZDC::ZdcAnalysisTool*)p);
   }
   static void deleteArray_ZDCcLcLZdcAnalysisTool(void *p) {
      delete [] ((::ZDC::ZdcAnalysisTool*)p);
   }
   static void destruct_ZDCcLcLZdcAnalysisTool(void *p) {
      typedef ::ZDC::ZdcAnalysisTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ZDC::ZdcAnalysisTool

namespace {
  void TriggerDictionaryInitialization_libZdcAnalysisDict_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libZdcAnalysisDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace ZDC{class __attribute__((annotate("$clingAutoload$ZdcAnalysis/ZdcAnalysisAlg.h")))  ZdcAnalysisAlg;}
namespace ZDC{class __attribute__((annotate("$clingAutoload$ZdcAnalysis/ZdcAnalysisTool.h")))  ZdcAnalysisTool;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libZdcAnalysisDict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "ZdcAnalysis-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ ZdcAnalysis-00-00-00
#endif
#ifndef EIGEN_DONT_VECTORIZE
  #define EIGEN_DONT_VECTORIZE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ZDCANALYSIS_ZDCANALYSIS_DICT_H
#define ZDCANALYSIS_ZDCANALYSIS_DICT_H

#include <ZdcAnalysis/ZdcAnalysisAlg.h>
#include <ZdcAnalysis/ZdcAnalysisTool.h>

#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"ZDC::ZdcAnalysisAlg", payloadCode, "@",
"ZDC::ZdcAnalysisTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libZdcAnalysisDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libZdcAnalysisDict_Impl, {{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1},{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libZdcAnalysisDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libZdcAnalysisDict() {
  TriggerDictionaryInitialization_libZdcAnalysisDict_Impl();
}
